#!/usr/bin/env bash



dnu restore --ignore-failed-sources -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/

# check if path is supplied, otherwise everything
WORKING_PATH="."

if [ -n "$1" ]
then
    WORKING_PATH="$1"
fi

echo "Specified PATH: $WORKING_PATH"


# build package for client project
for proj in `find "$WORKING_PATH" -maxdepth 0 -type d | grep -v "artifacts"`
do
    if test -f "$proj/project.json"
    then
        echo "Packing $proj:"
        # update project.json to reflect the latest build
        cat "$proj/project.json" | jq --arg current_build $BUILD_NUMBER '.version = .version + "-" + $current_build' > "$proj/project.tmp.json" 
        mv "$proj/project.tmp.json" "$proj/project.json"
        cat "$proj/project.json"
        dnu pack "$proj" --out ./artifacts --configuration Release
    fi
done


#read -n 1 -s