FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Cashflow.Abstractions /app/LendFoundry.Cashflow.Abstractions
WORKDIR /app/LendFoundry.Cashflow.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Cashflow.Persistence /app/LendFoundry.Cashflow.Persistence
WORKDIR /app/LendFoundry.Cashflow.Persistence
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Cashflow /app/LendFoundry.Cashflow
WORKDIR /app/LendFoundry.Cashflow
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Cashflow.Client /app/LendFoundry.Cashflow.Client
WORKDIR /app/LendFoundry.Cashflow.Client
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Cashflow.Api /app/LendFoundry.Cashflow.Api
WORKDIR /app/LendFoundry.Cashflow.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000

ENTRYPOINT dnx kestrel