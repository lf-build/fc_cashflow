﻿using LendFoundry.Foundation.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Cashflow.Tests
{
    public class CashflowStoreTests : InMemoryObjects
    {
        private CashflowStoreService Service => new CashflowStoreService
            (
                Logger.Object,
                StoreRepository.Object,
                EventHub.Object,
                TenantTime.Object,
                LookupService.Object,
                Configuration.Object,
                AnalysisService.Object
            );

        [Fact]
        public void AddOrUpdate_WhenExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Configuration.Setup(x => x.Get()).Returns(new CashflowConfiguration { LookupEntity = "Banks" });
            LookupService.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(InMemoryLookupEntry);
            TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
            StoreRepository
                .Setup(x => x.AddOrUpdateBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<BankAccountType>(), It.IsAny<IEnumerable<ITransaction>>()))
                .Returns(InMemoryBankAccount);

            var realRoutingNumber = InMemoryLookupEntry.First().Key;

            // action
            var result = Service.AddOrUpdateBankAccount(InMemoryAccountHolder, realRoutingNumber, "123456", BankAccountType.Checking, 0, new[] { InMemoryTransaction });

            // assert
            Assert.NotNull(result);
            Assert.NotNull(result.Transactions);
            Assert.NotEmpty(result.Transactions);
            Assert.Equal(result.RoutingNumber, realRoutingNumber);

            Configuration.Verify(x => x.Get(), Times.AtLeastOnce);
            LookupService
                .Verify(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            StoreRepository
                .Verify(x => x.AddOrUpdateBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<BankAccountType>(), It.IsAny<IEnumerable<ITransaction>>()), Times.AtLeastOnce);
        }

        [Fact]
        public void AddOrUpdate_When_HasNo_CashflowConfiguration()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
                StoreRepository
                    .Setup(x => x.AddOrUpdateBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<BankAccountType>(), It.IsAny<IEnumerable<ITransaction>>()))
                    .Returns(InMemoryBankAccount);
                var realRoutingNumber = InMemoryLookupEntry.First().Key;

                // action
                var result = Service.AddOrUpdateBankAccount(InMemoryAccountHolder, realRoutingNumber, "123456", BankAccountType.Checking, 0, new[] { InMemoryTransaction });

                // assert
                Assert.Null(result);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
                StoreRepository
                    .Verify(x => x.AddOrUpdateBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<BankAccountType>(), It.IsAny<IEnumerable<ITransaction>>()), Times.Never);
            });
        }

        [Fact]
        public void AddOrUpdate_When_HasNo_AccountHolder()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
                StoreRepository
                    .Setup(x => x.AddOrUpdateBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<BankAccountType>(), It.IsAny<IEnumerable<ITransaction>>()))
                    .Returns(InMemoryBankAccount);
                var realRoutingNumber = InMemoryLookupEntry.First().Key;

                // action
                var result = Service.AddOrUpdateBankAccount(null, realRoutingNumber, "123456", BankAccountType.Checking, 0, new[] { InMemoryTransaction });

                // assert
                Assert.Null(result);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
                StoreRepository
                    .Verify(x => x.AddOrUpdateBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<BankAccountType>(), It.IsAny<IEnumerable<ITransaction>>()), Times.Never);
            });
        }



        [Fact]
        public void GetListOfBankAccount_With_AccountHolder_ExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            StoreRepository.Setup(x => x.GetBankAccounts(It.IsAny<IAccountHolder>())).Returns(new[] { InMemoryBankAccount });

            Configuration.Setup(x => x.Get()).Returns(new CashflowConfiguration { LookupEntity = "Banks" });
            LookupService.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(InMemoryLookupEntry);
            TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
            StoreRepository
                .Setup(x => x.AddOrUpdateBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<BankAccountType>(), It.IsAny<IEnumerable<ITransaction>>()))
                .Returns(InMemoryBankAccount);

            var realRoutingNumber = InMemoryLookupEntry.First().Key;

            // action
            var bankAccount = Service.AddOrUpdateBankAccount(InMemoryAccountHolder, realRoutingNumber, "123456", BankAccountType.Checking, 0, new[] { InMemoryTransaction });
            var result = Service.GetBankAccounts(InMemoryAccountHolder);


            // assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(result.FirstOrDefault().AccountHolder, bankAccount.AccountHolder);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            StoreRepository.Verify(x => x.GetBankAccounts(It.IsAny<IAccountHolder>()), Times.AtLeastOnce);
        }

        [Fact]
        public void GetListOfBankAccount_With_AccountHolder_Without_AccountHolderId()
        {
            Assert.Throws<InvalidArgumentException>(() => 
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                
                // action
                var result = Service.GetBankAccounts(new AccountHolder { Type = "type" });
                // assert
                Assert.Null(result);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }



        [Fact]
        public void GetBankAccount_ExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            StoreRepository
                .Setup(x => x.GetBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(InMemoryBankAccount);

            // action
            var result = Service.GetBankAccount(InMemoryAccountHolder, "routingNumber", "accountNumber");

            // assert
            Assert.NotNull(result);
            Assert.Equal(result.AccountHolder, InMemoryBankAccount.AccountHolder);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            StoreRepository.Verify(x => x.GetBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void GetBankAccount_Without_RoutingNumber()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // action
                var result = Service.GetBankAccount(new AccountHolder { Type = "type" }, "routingNumber", "accountNumber");
                // assert
                Assert.Null(result);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }



        [Fact]
        public void GetTransaction_WhenExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            var currentTransaction = InMemoryBankAccount.Transactions.FirstOrDefault();
            currentTransaction.Id = Guid.NewGuid().ToString("N");

            StoreRepository
                .Setup(x => x.GetTransaction(It.IsAny<string>()))
                .Returns(currentTransaction);

            // action
            var result = Service.GetTransaction(currentTransaction.Id);

            // assert
            Assert.NotNull(result);
            Assert.Equal(currentTransaction.Id, currentTransaction.Id);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            StoreRepository.Verify(x => x.GetTransaction(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void GetTransaction_WhenExecutionFailed()
        {
            Assert.Throws<NotFoundException>(() => 
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                ITransaction transaction = null;
                StoreRepository
                    .Setup(x => x.GetTransaction(It.IsAny<string>()))
                    .Returns(transaction);

                // action
                var result = Service.GetTransaction("Id");

                // assert
                Assert.Null(result);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
                StoreRepository.Verify(x => x.GetTransaction(It.IsAny<string>()), Times.Never);
            });
        }



        [Fact]
        public void RemoveBankAccount_ExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            var currentTransaction = InMemoryBankAccount.Transactions.FirstOrDefault();
            currentTransaction.Id = Guid.NewGuid().ToString("N");

            StoreRepository
                .Setup(x => x.RemoveBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);

            // action
            Service.RemoveBankAccount(InMemoryAccountHolder, "routingNumber", "accountNumber");

            // assert
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            StoreRepository
                .Verify(x => x.RemoveBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void RemoveBankAccount_Without_RoutingNumber()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // action
                Service.RemoveBankAccount(new AccountHolder { Type = "type" }, "routingNumber", "accountNumber");
                
                // assert
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }



        [Fact]
        public void RemoveTransaction_WhenExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            var currentTransaction = InMemoryBankAccount.Transactions.FirstOrDefault();
            currentTransaction.Id = Guid.NewGuid().ToString("N");

            StoreRepository
                .Setup(x => x.RemoveTransaction(It.IsAny<ITransaction>()))
                .Returns(true);

            // action
            Service.RemoveTransaction(currentTransaction);

            // assert
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            StoreRepository.Verify(x => x.RemoveTransaction(It.IsAny<ITransaction>()), Times.AtLeastOnce);
        }

        [Fact]
        public void RemoveTransaction_WhenExecutionFailed()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                StoreRepository
                .Setup(x => x.RemoveTransaction(It.IsAny<ITransaction>()))
                .Throws<ArgumentNullException>();

                // action
                Service.RemoveTransaction(new Transaction());

                // assert
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
                StoreRepository.Verify(x => x.RemoveTransaction(It.IsAny<ITransaction>()), Times.AtLeastOnce);
            });
        }



        [Fact]
        public void UpdateTransaction_WhenExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            var currentTransaction = InMemoryBankAccount.Transactions.FirstOrDefault();
            currentTransaction.Id = Guid.NewGuid().ToString("N");

            StoreRepository
                .Setup(x => x.UpdateTransaction(It.IsAny<ITransaction>()))
                .Returns(true);

            // action
            Service.UpdateTransaction(currentTransaction);

            // assert
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            StoreRepository.Verify(x => x.UpdateTransaction(It.IsAny<ITransaction>()), Times.AtLeastOnce);
        }

        [Fact]
        public void UpdateTransaction_WhenExecutionFailed()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                StoreRepository
                .Setup(x => x.UpdateTransaction(It.IsAny<ITransaction>()))
                .Throws<ArgumentNullException>();

                // action
                Service.UpdateTransaction(new Transaction());

                // assert
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
                StoreRepository.Verify(x => x.UpdateTransaction(It.IsAny<ITransaction>()), Times.AtLeastOnce);
            });
        }
    }
}