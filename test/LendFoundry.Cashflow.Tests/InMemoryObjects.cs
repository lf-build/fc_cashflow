﻿using LendFoundry.Cashflow.Analysis;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using Moq;
using System;
using System.Collections.Generic;

namespace LendFoundry.Cashflow.Tests
{
    public class InMemoryObjects
    {
        protected Mock<ICashflowStoreRepository> StoreRepository { get; } = new Mock<ICashflowStoreRepository>();

        protected Mock<ICashflowAnalysisRepository> AnalysisRepository { get; } = new Mock<ICashflowAnalysisRepository>();

        protected Mock<IEventHubClient> EventHub { get; } = new Mock<IEventHubClient>();

        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();

        protected Mock<ICashflowStoreService> StoreService { get; } = new Mock<ICashflowStoreService>();

        protected Mock<ICashflowAnalysisServiceExtended> AnalysisService { get; } = new Mock<ICashflowAnalysisServiceExtended>();

        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        protected Mock<IConfigurationService<CashflowConfiguration>> Configuration { get; } = new Mock<IConfigurationService<CashflowConfiguration>>();

        protected Mock<ILookupService> LookupService { get; } = new Mock<ILookupService>();

        protected Mock<ICashflowAnalysisSet> CashflowAnalysisSet { get; } = new Mock<ICashflowAnalysisSet>();


        protected IAccountHolder InMemoryAccountHolder { get; } = new AccountHolder
        {
            Id = Guid.NewGuid().ToString("N"),
            Type = "AccountHolderType"
        };

        protected ITransaction InMemoryTransaction { get; } = new Transaction
        {
            Amount = 100.20,
            Category = "Category",
            CheckOrSlipNumber = "CheckOrSlipNumber",
            Date = new TimeBucket(DateTime.Now),
            Description = "Description Fake",
            MerchantName = "MerchantName",
            Type = TransactionType.Credit
        };

        protected Dictionary<string, string> InMemoryLookupEntry
        {
            get
            {
                var realRoutingNumber = "322078370";
                var realBankName = "LOS ANGELES FEDERAL CREDIT UNION";
                var lookupEntry = new Dictionary<string, string>();
                lookupEntry.Add(realRoutingNumber, realBankName);
                return lookupEntry;
            }
        }

        protected IBankAccount InMemoryBankAccount { get; } = new BankAccount
        {
            AccountHolder = new AccountHolder { Id = Guid.NewGuid().ToString("N"), Type = "AccountHolderType" },
            AccountNumber = "123456",
            AccountType = BankAccountType.Checking,
            BankName = "LOS ANGELES FEDERAL CREDIT UNION",
            RoutingNumber = "322078370",
            Transactions = new [] 
            {
                new Transaction
                {
                    Amount = 2000.00,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 05, 01)),
                    Description = "Paycheck",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Credit
                },
                new Transaction
                {
                    Amount = 1200.00,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 05, 05)),
                    Description = "Rent",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Debit
                },
                new Transaction
                {
                    Amount = 250.00,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 05, 10)),
                    Description = "Wallmart",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Debit
                },
                new Transaction
                {
                    Amount = 45.00,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 05, 22)),
                    Description = "Deposit",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Credit
                },
                new Transaction
                {
                    Amount = 300,
                    Category = "Unknown",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 05, 27)),
                    Description = "Gato Preto (party house)",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Debit
                },
                new Transaction
                {
                    Amount = 2000,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 06, 01)),
                    Description = "Paycheck",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Credit
                },
                new Transaction
                {
                    Amount = 80,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 06, 04)),
                    Description = "SLE Light",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Debit
                },
                new Transaction
                {
                    Amount = 1200.00,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016,06, 05)),
                    Description = "Rent",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Debit
                }
            }
        };

        protected IMonthlyCashflowSummary InMemoryCashflowSummary { get; } = new CashflowSummary
        {
            AverageDailyBalance = 0.0,
            BeginningBalance = 0.0,
            EndingBalance = 0.0,
            Month = DateTime.Today.Month,
            TotalDepositAmount = 0.0,
            TotalFees = 0.0,
            TotalNegativeDays = 0,
            TotalNonSufficientFundFees = 0.0,
            TotalNumberOfDeposits = 0,
            TotalNumberOfWithdrawals = 0,
            TotalWithdrawalAmount = 0.0,
            Year = DateTime.Today.Year
        };

        protected IBankAccountSummary InMemoryBankAccountSummary { get; } = new BankAccountSummary
        {
            AccountHolder = new AccountHolder { Id = Guid.NewGuid().ToString("N"), Type = "AccountHolderType" },
            AccountNumber = "123456",
            BankName = "LOS ANGELES FEDERAL CREDIT UNION",
            RoutingNumber = "322078370",
            AverageDailyBalance = 0.0,
            BeginningBalance = 0.0,
            EndingBalance = 0.0,
            TotalDepositAmount = 0.0,
            TotalFees = 0.0,
            TotalNegativeDays = 0,
            TotalNonSufficientFundFees = 0.0,
            TotalNumberOfDeposits = 0,
            TotalNumberOfWithdrawals = 0,
            TotalWithdrawalAmount = 0.0
        };
    }
}