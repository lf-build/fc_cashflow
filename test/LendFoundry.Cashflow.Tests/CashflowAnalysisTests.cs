﻿using LendFoundry.Cashflow.Analysis;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;

namespace LendFoundry.Cashflow.Tests
{
    public class CashflowAnalysisTests : InMemoryObjects
    {
        private CashflowService Service => new CashflowService
            (
                Logger.Object,
                AnalysisRepository.Object,
                EventHub.Object,
                TenantTime.Object,
                LookupService.Object,
                Configuration.Object,
                new CashflowAnalysisSet
                {
                    new TotalDepositAmountAnalysis(),
                    new TotalWidrawalAmountAnalsysis(),
                    new TotalNumberOfDepositsAnalysis(),
                    new TotalNumberOfWithdrawalsAnalysis(),
                    new AverageDailyBalanceAnalysis(),
                    new TotalNegativeDaysAnalysis(),
                    new BeginningBalanceAnalysis(),
                    new EndingBalanceAnalysis(),
                    new TotalFeesAnalysis(),
                    new TotalNonSufficientFundFeesAnalysis()
                }
            );


        [Fact]
        public void AddOrUpdateSummary_WhenExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Configuration.Setup(x => x.Get()).Returns(new CashflowConfiguration { LookupEntity = "Banks" });
            LookupService.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(InMemoryLookupEntry);
            TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
            AnalysisRepository
                .Setup(x => x.AddOrUpdate(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IMonthlyCashflowSummary>()))
                .Returns(InMemoryCashflowSummary);

            var realRoutingNumber = InMemoryLookupEntry.First().Key;

            // action
            var result = Service.AddOrUpdateSummary(InMemoryAccountHolder, realRoutingNumber, "123456", InMemoryCashflowSummary);

            // assert
            Assert.NotNull(result);
            Assert.Equal(result.Month, InMemoryCashflowSummary.Month);
            Assert.Equal(result.Year, InMemoryCashflowSummary.Year);

            Configuration.Verify(x => x.Get(), Times.AtLeastOnce);
            LookupService
                .Verify(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            AnalysisRepository
                .Verify(x => x.AddOrUpdate(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IMonthlyCashflowSummary>()), Times.AtLeastOnce);
        }

        [Fact]
        public void AddOrUpdateSummary_When_HasNo_CashflowConfiguration()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
                AnalysisRepository
                    .Setup(x => x.AddOrUpdate(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IMonthlyCashflowSummary>()))
                    .Returns(InMemoryCashflowSummary);
                var realRoutingNumber = InMemoryLookupEntry.First().Key;

                // action
                var result = Service.AddOrUpdateSummary(InMemoryAccountHolder, realRoutingNumber, "123456", InMemoryCashflowSummary);

                // assert
                Assert.Null(result);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
                AnalysisRepository
                    .Verify(x => x.AddOrUpdate(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IMonthlyCashflowSummary>()), Times.Never);
            });
        }

        [Fact]
        public void AddOrUpdateSummary_When_HasNo_AccountHolder()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));
                AnalysisRepository
                    .Setup(x => x.AddOrUpdate(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IMonthlyCashflowSummary>()))
                    .Returns(InMemoryCashflowSummary);
                var realRoutingNumber = InMemoryLookupEntry.First().Key;

                // action
                var result = Service.AddOrUpdateSummary(null, realRoutingNumber, "123456", InMemoryCashflowSummary);

                // assert
                Assert.Null(result);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
                AnalysisRepository
                    .Verify(x => x.AddOrUpdate(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IMonthlyCashflowSummary>()), Times.Never);
            });
        }



        [Fact]
        public void GetBankAccountSummaries_With_AccountHolder_ExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            AnalysisRepository.Setup(x => x.GetBankAccountSummaries(It.IsAny<IAccountHolder>()))
                .Returns(new[] { InMemoryBankAccountSummary });

            // action
            var result = Service.GetBankAccountSummaries(InMemoryAccountHolder);

            // assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(result.FirstOrDefault().AccountHolder, InMemoryBankAccountSummary.AccountHolder);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            AnalysisRepository.Verify(x => x.GetBankAccountSummaries(It.IsAny<IAccountHolder>()), Times.AtLeastOnce);
        }

        [Fact]
        public void GetBankAccountSummaries_With_AccountHolder_Without_AccountHolderId()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // action
                var result = Service.GetBankAccountSummaries(new AccountHolder { Type = "type" });
                // assert
                Assert.Null(result);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }



        [Fact]
        public void GetBankAccountSummary_ExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            AnalysisRepository
                .Setup(x => x.GetBankAccountSummary(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(InMemoryBankAccountSummary);

            // action
            var result = Service.GetBankAccountSummary(InMemoryAccountHolder, "routingNumber", "accountNumber");

            // assert
            Assert.NotNull(result);
            Assert.Equal(result.AccountHolder, InMemoryBankAccountSummary.AccountHolder);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            AnalysisRepository.Verify(x => x.GetBankAccountSummary(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void GetBankAccountSummary_Without_RoutingNumber()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // action
                var result = Service.GetBankAccountSummary(new AccountHolder { Type = "type" }, "routingNumber", "accountNumber");
                // assert
                Assert.Null(result);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }



        [Fact]
        public void GetSummary_WhenExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            AnalysisRepository
                .Setup(x => x.GetSummary(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(InMemoryCashflowSummary);

            // action
            var result = Service.GetSummary
            (
                InMemoryBankAccountSummary.AccountHolder,
                InMemoryBankAccountSummary.AccountNumber,
                InMemoryBankAccountSummary.RoutingNumber,
                DateTime.Today.Year,
                DateTime.Today.Month
            );

            // assert
            Assert.NotNull(result);
            Assert.Equal(result.Year, InMemoryCashflowSummary.Year);
            Assert.Equal(result.Month, InMemoryCashflowSummary.Month);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            AnalysisRepository
                .Verify(x => x.GetSummary(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.AtLeastOnce);
        }

        [Fact]
        public void GetSummary_WhenExecutionFailed()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                AnalysisRepository
                    .Setup(x => x.GetSummary(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(InMemoryCashflowSummary);


                // action
                var result = Service.GetSummary
                (
                    new AccountHolder(),
                    InMemoryBankAccountSummary.AccountNumber,
                    InMemoryBankAccountSummary.RoutingNumber,
                    DateTime.Today.Year,
                    DateTime.Today.Month
                );

                // assert
                Assert.Null(result);
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
                AnalysisRepository
                    .Verify(x => x.GetSummary(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.Never);
            });
        }



        [Fact]
        public void RemoveSummary_ExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            AnalysisRepository
                .Setup(x => x.RemoveSummary(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(true);

            // action
            Service.RemoveSummary
            (
                InMemoryBankAccountSummary.AccountHolder,
                InMemoryBankAccountSummary.AccountNumber,
                InMemoryBankAccountSummary.RoutingNumber,
                DateTime.Today.Year,
                DateTime.Today.Month
            );

            // assert
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            AnalysisRepository
                .Verify(x => x.RemoveSummary(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.AtLeastOnce);
        }

        [Fact]
        public void RemoveSummary_Without_RoutingNumber()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // action
                Service.RemoveSummary(new AccountHolder { Type = "type" }, "routingNumber", "accountNumber", DateTime.Today.Year, DateTime.Today.Month);

                // assert
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
            });
        }


        [Fact]
        public void GenerateCalculations_When_ExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Configuration.Setup(x => x.Get()).Returns(new CashflowConfiguration { LookupEntity = "Banks" });
            LookupService.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(InMemoryLookupEntry);
            TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));

            AnalysisRepository
                .Setup(x => x.GetBankAccountSummary(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns<IBankAccountSummary>(null);

            AnalysisRepository
                .Setup(x => x.AddOrUpdateBankAccountSummary(It.IsAny<IBankAccountSummary>()));

            var realRoutingNumber = InMemoryLookupEntry.First().Key;

            var currentBalance = 0.0;
            var updatedTransactions = InMemoryBankAccount.Transactions.Select(transaction =>
            {
                transaction.Id = Guid.NewGuid().ToString("N");

                // making sure values are absolute
                transaction.Amount = Math.Abs(transaction.Amount);

                // setting the running balance based on the beginning balance
                currentBalance = transaction.Type == TransactionType.Debit
                    ? currentBalance - transaction.Amount
                    : currentBalance + transaction.Amount;

                transaction.RunningBalanceAmount = currentBalance;

                return transaction;
            }).ToList();

            InMemoryBankAccount.Transactions = updatedTransactions;

            // action
            var result = Service.Generate(InMemoryBankAccount);

            // assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);

            var firstMonth = result.OrderBy(m => m.Month).FirstOrDefault();
            Assert.Equal(9.5161290322580641, firstMonth.AverageDailyBalance);
            Assert.Equal(0, firstMonth.BeginningBalance);
            Assert.Equal(295, firstMonth.EndingBalance);
            Assert.Equal(5, firstMonth.Month);
            Assert.Equal(2045, firstMonth.TotalDepositAmount);
            Assert.Equal(0, firstMonth.TotalNegativeDays);
            Assert.Equal(2, firstMonth.TotalNumberOfDeposits);
            Assert.Equal(3, firstMonth.TotalNumberOfWithdrawals);
            Assert.Equal(1750, firstMonth.TotalWithdrawalAmount);

            var secondMonth = result.Where(m => m != firstMonth).FirstOrDefault();
            Assert.Equal(33.833333333333336, secondMonth.AverageDailyBalance);
            Assert.Equal(295, secondMonth.BeginningBalance);
            Assert.Equal(1015, secondMonth.EndingBalance);
            Assert.Equal(6, secondMonth.Month);
            Assert.Equal(2000, secondMonth.TotalDepositAmount);
            Assert.Equal(0, secondMonth.TotalNegativeDays);
            Assert.Equal(1, secondMonth.TotalNumberOfDeposits);
            Assert.Equal(2, secondMonth.TotalNumberOfWithdrawals);
            Assert.Equal(1280, secondMonth.TotalWithdrawalAmount);

            Configuration.Verify(x => x.Get(), Times.AtLeastOnce);
            LookupService
                .Verify(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            AnalysisRepository
                .Verify(x => x.AddOrUpdateBankAccountSummary(It.IsAny<IBankAccountSummary>()), Times.AtLeastOnce);
        }

        [Fact]
        public void PerformanceTest_CashflowAnalysis()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Configuration.Setup(x => x.Get()).Returns(new CashflowConfiguration { LookupEntity = "Banks" });
            LookupService.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(InMemoryLookupEntry);
            TenantTime.Setup(x => x.Now).Returns(new DateTimeOffset(DateTime.Now));

            var analysis = new CashflowService
            (
                Logger.Object,
                AnalysisRepository.Object,
                EventHub.Object,
                TenantTime.Object,
                LookupService.Object,
                Configuration.Object,
                new CashflowAnalysisSet
                {
                    new TotalDepositAmountAnalysis(),
                    new TotalWidrawalAmountAnalsysis(),
                    new TotalNumberOfDepositsAnalysis(),
                    new TotalNumberOfWithdrawalsAnalysis(),
                    new AverageDailyBalanceAnalysis(),
                    new TotalNegativeDaysAnalysis(),
                    new BeginningBalanceAnalysis(),
                    new EndingBalanceAnalysis(),
                    new TotalFeesAnalysis(),
                    new TotalNonSufficientFundFeesAnalysis()
                }
            );

            var store = new CashflowStoreService
            (
                Logger.Object,
                StoreRepository.Object,
                EventHub.Object,
                TenantTime.Object,
                LookupService.Object,
                Configuration.Object,
                analysis
            );

            var transactions = InMemoryBankAccount.Transactions.Where(t => t.Date.Time.Month == 05).ToList();
            var annualTransactions = new List<ITransaction>();

            // per year (60) transactions
            // per year with {2010, 2011, .... , 2016}
            for (int year = 1996; year <= 2016; year++)
            {
                for (int month = 1; month <= 12; month++)
                {
                    annualTransactions.AddRange
                    (
                        transactions.Select(transaction =>
                        {
                            var date = new TimeBucket(
                                new DateTimeOffset(
                                    year,
                                    month,
                                    transaction.Date.Time.Day,
                                    transaction.Date.Time.Hour,
                                    transaction.Date.Time.Minute,
                                    transaction.Date.Time.Second,
                                    transaction.Date.Time.Offset));

                            transaction.Date = date;
                            return transaction;
                        }).ToList()
                    );
                }
            }

            InMemoryBankAccount.Transactions = annualTransactions;

            StoreRepository
                .Setup(x => x.AddOrUpdateBankAccount(It.IsAny<IAccountHolder>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<BankAccountType>(), It.IsAny<IEnumerable<ITransaction>>()))
                .Returns(InMemoryBankAccount);

            var logEntries = new StringBuilder();

            // action
            // ------------------------------------ Complete process (add BankAccount and Execute the Calculations)
            var performCount = new Stopwatch();
            logEntries.AppendLine($"{DateTime.Now} - AddOrUpdateBankAccount (method) using {{ { InMemoryBankAccount.Transactions.Count() } }} transactions");
            performCount.Start();
            var bankAccount = store.AddOrUpdateBankAccount
            (
                InMemoryBankAccount.AccountHolder,
                InMemoryBankAccount.RoutingNumber,
                InMemoryBankAccount.AccountNumber,
                InMemoryBankAccount.AccountType,
                0,
                InMemoryBankAccount.Transactions
            );
            performCount.Stop();
            logEntries.AppendLine($"{DateTime.Now} - AddOrUpdateBankAccount (method) elapsed time : { performCount.Elapsed }");

            //---------------------------------- Generate Calculation Only
            logEntries.AppendLine($"{DateTime.Now} - Generate (method) using {{ { InMemoryBankAccount.Transactions.Count() } }} transactions");
            performCount = new Stopwatch();
            performCount.Start();
            var bankAccountSummary = analysis.Generate(InMemoryBankAccount);
            performCount.Stop();
            logEntries.AppendLine($"{DateTime.Now} - Generate (method) elapsed time : { performCount.Elapsed }");


            //---------------------------------- Write to Log file (.txt)
            var path = $"{string.Join<string>("\\", Environment.CurrentDirectory.Split('\\').TakeWhile(p => p != "test"))}\\artifacts\\PerformanceCounter.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(logEntries);
                // Free memory.
                writer.Flush();
                // Close de file.
                writer.Close();
            }

            // assert
            Configuration.Verify(x => x.Get(), Times.AtLeastOnce);
            LookupService
                .Verify(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
        }


        private static bool ValuesMatch(double expected, double actual)
        {
            return Math.Abs(expected - actual) < double.Epsilon;
        }
    }
}