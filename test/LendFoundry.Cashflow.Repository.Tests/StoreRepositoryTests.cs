﻿using LendFoundry.Cashflow.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Tenant.Client;
using Moq;
using System.Linq;
using Xunit;

namespace LendFoundry.Cashflow.Repository.Tests
{
    public class StoreRepositoryTests : InMemoryObjects
    {
        private ICashflowStoreRepository Repository(IMongoConfiguration config)
        {
            TenantService = new Mock<ITenantService>();
            TenantService.Setup(s => s.Current).Returns(new TenantInfo { Id = TenantId });
            return new CashflowStoreRepository
            (
                configuration: config,
                tenantService: TenantService.Object
            );
        }

        [MongoFact]
        public void AddOrUpdateBankAccount()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var list = new[] { InMemoryBankAccount }
                .Select(bankAccount =>
                {
                    bankAccount.TenantId = TenantId;
                    return bankAccount;
                }).ToArray();

                var currentItem = list.FirstOrDefault();
                // action
                var repository = Repository(config);
                var result = repository.AddOrUpdateBankAccount
                (
                    currentItem.AccountHolder,
                    currentItem.RoutingNumber,
                    currentItem.AccountNumber,
                    currentItem.BankName,
                    currentItem.AccountType,
                    currentItem.Transactions
                );

                // assert
                Assert.NotNull(result);
                Assert.NotNull(result.Id);
                Assert.NotEmpty(result.Transactions);
            });
        }

        [MongoFact]
        public void GetBankAccount()
        {
            MongoTest.Run(config =>
            {
                // arrange
                // action
                var repository = Repository(config);
                repository.AddOrUpdateBankAccount
                (
                    InMemoryBankAccount.AccountHolder,
                    InMemoryBankAccount.RoutingNumber,
                    InMemoryBankAccount.AccountNumber,
                    InMemoryBankAccount.BankName,
                    InMemoryBankAccount.AccountType,
                    InMemoryBankAccount.Transactions
                );

                var result = repository.GetBankAccount
                (
                    InMemoryBankAccount.AccountHolder, 
                    InMemoryBankAccount.RoutingNumber, 
                    InMemoryBankAccount.AccountNumber
                );

                // assert
                Assert.NotNull(result);
                Assert.NotNull(result.Id);
                Assert.NotEmpty(result.Transactions);
            });
        }

        [MongoFact]
        public void GetBankAccounts()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repository = Repository(config);
                var list = new[] { InMemoryBankAccount, InMemoryBankAccount,
                    InMemoryBankAccount, InMemoryBankAccount, InMemoryBankAccount, InMemoryBankAccount,
                    InMemoryBankAccount, InMemoryBankAccount, InMemoryBankAccount, InMemoryBankAccount
                }.Select((bankAccount, i) =>
                {
                    i++;
                    bankAccount.TenantId = TenantId;
                    bankAccount.AccountNumber = $"AccountNumber_{i}";
                    bankAccount.RoutingNumber = $"RoutingNumber_{i}";

                    return repository.AddOrUpdateBankAccount
                    (
                        bankAccount.AccountHolder,
                        bankAccount.RoutingNumber,
                        bankAccount.AccountNumber,
                        bankAccount.BankName,
                        bankAccount.AccountType,
                        bankAccount.Transactions
                    );

                    //return bankAccount;
                });

                // action
                var count = list.Count();
                var sixthItem = list.Take(6).LastOrDefault();
                var result = repository.GetBankAccounts(sixthItem.AccountHolder);

                // assert
                Assert.NotNull(result);
                Assert.Equal(count, result.ToList().Count);
            });
        }

        [MongoFact]
        public void GetTransaction()
        {
            MongoTest.Run(config =>
            {
                // arrange
                // action
                var repository = Repository(config);
                var bankAccountAdded = repository.AddOrUpdateBankAccount
                (
                    InMemoryBankAccount.AccountHolder,
                    InMemoryBankAccount.RoutingNumber,
                    InMemoryBankAccount.AccountNumber,
                    InMemoryBankAccount.BankName,
                    InMemoryBankAccount.AccountType,
                    InMemoryBankAccount.Transactions
                );

                var transactionId = bankAccountAdded.Transactions.FirstOrDefault().Id;
                var result = repository.GetTransaction(transactionId);

                // assert
                Assert.NotNull(result);
                Assert.NotNull(result.Id);
                Assert.Equal(transactionId, result.Id);
            });
        }

        [MongoFact]
        public void RemoveBankAccount()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repository = Repository(config);
                var list = new[] { InMemoryBankAccount, InMemoryBankAccount, InMemoryBankAccount }
                .Select((bankAccount, i) =>
                {
                    i++;
                    bankAccount.TenantId = TenantId;
                    bankAccount.AccountNumber = $"AccountNumber{i}";
                    bankAccount.RoutingNumber = $"RoutingNumber{i}";

                    return repository.AddOrUpdateBankAccount
                    (
                        bankAccount.AccountHolder,
                        bankAccount.RoutingNumber,
                        bankAccount.AccountNumber,
                        bankAccount.BankName,
                        bankAccount.AccountType,
                        bankAccount.Transactions
                    );

                    //return bankAccount;
                });

                // action
                var lastBankAccount = list.ToList().Take(2).LastOrDefault();
                var result = repository.RemoveBankAccount(lastBankAccount.AccountHolder, lastBankAccount.RoutingNumber, lastBankAccount.AccountNumber);

                // assert
                Assert.NotNull(result);
                Assert.True(result);
            });
        }

        [MongoFact]
        public void RemoveTransaction()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repository = Repository(config);
                var list = new[] { InMemoryBankAccount, InMemoryBankAccount, InMemoryBankAccount }
                .Select((bankAccount, i) =>
                {
                    i++;
                    bankAccount.TenantId = TenantId;
                    bankAccount.AccountNumber = $"AccountNumber{i}";
                    bankAccount.RoutingNumber = $"RoutingNumber{i}";

                    return repository.AddOrUpdateBankAccount
                    (
                        bankAccount.AccountHolder,
                        bankAccount.RoutingNumber,
                        bankAccount.AccountNumber,
                        bankAccount.BankName,
                        bankAccount.AccountType,
                        bankAccount.Transactions
                    );
                }).ToList();

                var lastBankAccount = list.ToList().Take(2).LastOrDefault();

                // action
                var transactionId = lastBankAccount.Transactions.FirstOrDefault().Id;
                var result = repository.GetTransaction(transactionId);

                Assert.NotNull(result);
                Assert.NotNull(result.Id);
                var returnRemove = repository.RemoveTransaction(result);

                // assert
                Assert.True(returnRemove);
            });
        }

        [MongoFact]
        public void UpdateTransaction()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repository = Repository(config);
                var list = new[] { InMemoryBankAccount, InMemoryBankAccount, InMemoryBankAccount }
                .Select((bankAccount, i) =>
                {
                    i++;
                    bankAccount.TenantId = TenantId;
                    bankAccount.AccountNumber = $"AccountNumber{i}";
                    bankAccount.RoutingNumber = $"RoutingNumber{i}";

                    return repository.AddOrUpdateBankAccount
                    (
                        bankAccount.AccountHolder,
                        bankAccount.RoutingNumber,
                        bankAccount.AccountNumber,
                        bankAccount.BankName,
                        bankAccount.AccountType,
                        bankAccount.Transactions
                    );
                }).ToList();

                var lastBankAccount = list.ToList().Take(2).LastOrDefault();

                // action
                var transactionToUpdate = lastBankAccount.Transactions.FirstOrDefault();
                transactionToUpdate.Description = "Updated";
                var result = repository.UpdateTransaction(transactionToUpdate);

                // assert
                Assert.True(result);
            });
        }
    }
}