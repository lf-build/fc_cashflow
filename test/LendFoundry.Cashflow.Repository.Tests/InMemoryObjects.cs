﻿using LendFoundry.Foundation.Date;
using LendFoundry.Tenant.Client;
using Moq;
using System;

namespace LendFoundry.Cashflow.Repository.Tests
{
    public abstract class InMemoryObjects
    {
        public string TenantId { get; } = "my-tenant";

        protected Mock<ITenantService> TenantService { get; set; } = new Mock<ITenantService>();

        protected IAccountHolder InMemoryAccountHolder { get; } = new AccountHolder
        {
            Id = Guid.NewGuid().ToString("N"),
            Type = "AccountHolderType"
        };

        protected ITransaction InMemoryTransaction { get; } = new Transaction
        {
            Amount = 100.20,
            Category = "Category",
            CheckOrSlipNumber = "CheckOrSlipNumber",
            Date = new TimeBucket(DateTime.Now),
            Description = "Description Fake",
            MerchantName = "MerchantName",
            Type = TransactionType.Credit
        };

        protected IBankAccount InMemoryBankAccount { get; } = new BankAccount
        {
            AccountHolder = new AccountHolder { Id = Guid.NewGuid().ToString("N"), Type = "AccountHolderType" },
            AccountNumber = "123456",
            AccountType = BankAccountType.Checking,
            BankName = "LOS ANGELES FEDERAL CREDIT UNION",
            RoutingNumber = "322078370",
            Transactions = new[]
            {
                new Transaction
                {
                    Amount = 2000.00,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 05, 01)),
                    Description = "Paycheck",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Credit
                },
                new Transaction
                {
                    Amount = 1200.00,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 05, 05)),
                    Description = "Rent",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Debit
                },
                new Transaction
                {
                    Amount = 250.00,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 05, 10)),
                    Description = "Wallmart",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Debit
                },
                new Transaction
                {
                    Amount = 45.00,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 05, 22)),
                    Description = "Deposit",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Credit
                },
                new Transaction
                {
                    Amount = 300,
                    Category = "Unknown",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 05, 27)),
                    Description = "Gato Preto (party house)",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Debit
                },
                new Transaction
                {
                    Amount = 2000,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 06, 01)),
                    Description = "Paycheck",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Credit
                },
                new Transaction
                {
                    Amount = 80,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016, 06, 04)),
                    Description = "SLE Light",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Debit
                },
                new Transaction
                {
                    Amount = 1200.00,
                    Category = "Category",
                    CheckOrSlipNumber = "CheckOrSlipNumber",
                    Date = new TimeBucket(new DateTime(2016,06, 05)),
                    Description = "Rent",
                    MerchantName = "MerchantName",
                    Type = TransactionType.Debit
                }
            }
        };

        protected IMonthlyCashflowSummary InMemoryCashflowSummary { get; } = new CashflowSummary
        {
            AverageDailyBalance = 0.0,
            BeginningBalance = 0.0,
            EndingBalance = 0.0,
            Month = DateTime.Today.Month,
            TotalDepositAmount = 0.0,
            TotalFees = 0.0,
            TotalNegativeDays = 0,
            TotalNonSufficientFundFees = 0.0,
            TotalNumberOfDeposits = 0,
            TotalNumberOfWithdrawals = 0,
            TotalWithdrawalAmount = 0.0,
            Year = DateTime.Today.Year
        };

        protected IBankAccountSummary InMemoryBankAccountSummary { get; } = new BankAccountSummary
        {
            AccountHolder = new AccountHolder { Id = Guid.NewGuid().ToString("N"), Type = "AccountHolderType" },
            AccountNumber = "123456",
            BankName = "LOS ANGELES FEDERAL CREDIT UNION",
            RoutingNumber = "322078370",
            AverageDailyBalance = 0.0,
            BeginningBalance = 0.0,
            EndingBalance = 0.0,
            TotalDepositAmount = 0.0,
            TotalFees = 0.0,
            TotalNegativeDays = 0,
            TotalNonSufficientFundFees = 0.0,
            TotalNumberOfDeposits = 0,
            TotalNumberOfWithdrawals = 0,
            TotalWithdrawalAmount = 0.0
        };
    }
}