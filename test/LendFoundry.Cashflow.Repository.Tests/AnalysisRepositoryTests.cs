﻿using LendFoundry.Cashflow.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Driver;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace LendFoundry.Cashflow.Repository.Tests
{
    public class AnalysisRepositoryTests : InMemoryObjects
    {
        private ICashflowAnalysisRepository AnalysisRepository(IMongoConfiguration config)
        {
            TenantService = new Mock<ITenantService>();
            TenantService.Setup(s => s.Current).Returns(new TenantInfo { Id = TenantId });
            return new CashflowAnalysisRepository
            (
                configuration: config,
                tenantService: TenantService.Object
            );
        }

        [MongoFact]
        public void AddOrUpdate()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var list = new[] { InMemoryBankAccount }
                .Select((bankAccount, i) =>
                {
                    i++;
                    bankAccount.TenantId = TenantId;
                    bankAccount.AccountHolder.Id = Guid.NewGuid().ToString("N");
                    bankAccount.AccountNumber = $"AccountNumber_a{i}";
                    bankAccount.RoutingNumber = $"RoutingNumber_a{i}";
                    return bankAccount;
                }).ToArray();

                var currentItem = list.FirstOrDefault();
                // action
                var repository = AnalysisRepository(config);
                var result = repository.AddOrUpdate
                (
                    currentItem.AccountHolder,
                    currentItem.RoutingNumber,
                    currentItem.AccountNumber,
                    currentItem.BankName,
                    InMemoryCashflowSummary
                );

                // assert
                Assert.NotNull(result);
            });
        }


        [MongoFact]
        public void AddOrUpdateBankAccountSummary()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var list = new[] { InMemoryBankAccount }
                .Select((bankAccount, i) =>
                {
                    i++;
                    bankAccount.TenantId = TenantId;
                    bankAccount.AccountHolder.Id = Guid.NewGuid().ToString("N");
                    bankAccount.AccountNumber = $"AccountNumber_a{i}";
                    bankAccount.RoutingNumber = $"RoutingNumber_a{i}";
                    return bankAccount;
                }).ToArray();

                var currentItem = list.FirstOrDefault();
                // action
                var repository = AnalysisRepository(config);
                repository.AddOrUpdateBankAccountSummary
                (
                    new BankAccountSummary
                    {
                        AccountHolder = currentItem.AccountHolder,
                        RoutingNumber = currentItem.RoutingNumber,
                        AccountNumber = currentItem.AccountNumber,
                        BankName = currentItem.BankName,
                        Entries = new[] { new CashflowSummary
                                            {
                                                AverageDailyBalance = 0.0,
                                                BeginningBalance = 2000.0,
                                                EndingBalance = 6000.0,
                                                Month = DateTime.Today.Month,
                                                TotalDepositAmount = 0.0,
                                                TotalFees = 0.0,
                                                TotalNegativeDays = 0,
                                                TotalNonSufficientFundFees = 0.0,
                                                TotalNumberOfDeposits = 0,
                                                TotalNumberOfWithdrawals = 0,
                                                TotalWithdrawalAmount = 0.0,
                                                Year = DateTime.Today.Year
                                            }
                        }
                    }
                );

                var summary = repository.GetSummary
                (
                    currentItem.AccountHolder,
                    currentItem.RoutingNumber,
                    currentItem.AccountNumber,
                    DateTime.Now.Year,
                    DateTime.Now.Month
                );

                // assert
                Assert.NotNull(summary);
                Assert.Equal(2000.0, summary.BeginningBalance);
            });
        }


        [MongoFact]
        public void GetBankAccountSummaries()
        {

        }


        [MongoFact]
        public void GetBankAccountSummary()
        {

        }


        [MongoFact]
        public void GetSummary()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var list = new[] { InMemoryBankAccount }
                .Select((bankAccount, i) =>
                {
                    i++;
                    bankAccount.TenantId = TenantId;
                    bankAccount.AccountHolder.Id = Guid.NewGuid().ToString("N");
                    bankAccount.AccountNumber = $"AccountNumber_a{i}";
                    bankAccount.RoutingNumber = $"RoutingNumber_a{i}";
                    return bankAccount;
                }).ToArray();

                var currentItem = list.FirstOrDefault();
                // action
                var repository = AnalysisRepository(config);
                var result = repository.AddOrUpdate
                (
                    currentItem.AccountHolder,
                    currentItem.RoutingNumber,
                    currentItem.AccountNumber,
                    currentItem.BankName,
                    InMemoryCashflowSummary
                );

                Assert.NotNull(result);
                var summary = repository.GetSummary
                (
                    currentItem.AccountHolder,
                    currentItem.RoutingNumber,
                    currentItem.AccountNumber,
                    DateTime.Now.Year,
                    DateTime.Now.Month
                );

                // assert
                Assert.NotNull(summary);
            });
        }


        [MongoFact]
        public void RemoveSummary()
        {

        }
    }
}