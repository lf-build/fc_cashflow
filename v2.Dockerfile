FROM microsoft/aspnetcore-build AS build-env
WORKDIR /app
# This is temporary as we don't have nuget server
ADD ./temp/NuGet2.0 /app/NuGet2.0

ADD ./src/LendFoundry.Cashflow.Abstractions /app/LendFoundry.Cashflow.Abstractions
WORKDIR /app/LendFoundry.Cashflow.Abstractions
RUN rm *.xproj
RUN dotnet restore -s /app/NuGet2.0/packages -s https://api.nuget.org/v3/index.json
RUN dotnet build

ADD ./src/LendFoundry.Cashflow /app/LendFoundry.Cashflow
WORKDIR /app/LendFoundry.Cashflow
RUN rm *.xproj
RUN dotnet restore  -s /app/NuGet2.0/packages -s https://api.nuget.org/v3/index.json
RUN dotnet build 

ADD ./src/LendFoundry.Cashflow.Persistence /app/LendFoundry.Cashflow.Persistence
WORKDIR /app/LendFoundry.Cashflow.Persistence
RUN rm *.xproj
RUN dotnet restore -s /app/NuGet2.0/packages -s https://api.nuget.org/v3/index.json
RUN dotnet build


ADD ./src/LendFoundry.Cashflow.Api /app/LendFoundry.Cashflow.Api
WORKDIR /app/LendFoundry.Cashflow.Api
RUN rm *.xproj
RUN dotnet restore -s /app/NuGet2.0/packages -s https://api.nuget.org/v3/index.json
RUN dotnet build

# Copy csproj and restore as distinct layers

WORKDIR /app/LendFoundry.Cashflow.Api
# Copy everything else and build
RUN dotnet publish -c Release -o out --no-restore

# Build runtime image
FROM microsoft/aspnetcore:2.0
WORKDIR /app/
COPY --from=build-env /app/LendFoundry.Cashflow.Api/out .
LABEL lendfoundry.branch "LENDFOUNDRY_BRANCH"
LABEL lendfoundry.commit "LENDFOUNDRY_COMMIT"
LABEL lendfoundry.build "LENDFOUNDRY_BUILD_NUMBER"
LABEL lendfoundry.tag "LENDFOUNDRY_TAG"
ENTRYPOINT ["dotnet", "LendFoundry.Cashflow.Api.dll"]