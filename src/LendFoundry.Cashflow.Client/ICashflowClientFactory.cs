using LendFoundry.Security.Tokens;

namespace LendFoundry.Cashflow.Client
{
    public interface ICashflowClientFactory
    {
        ICashflowService Create(ITokenReader reader);
    }
}