using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif


namespace LendFoundry.Cashflow.Client
{
    public static class CashflowClientExtensions
    {
        public static IServiceCollection AddCashflowService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ICashflowClientFactory>(p => new CashflowClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ICashflowClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }
    }
}