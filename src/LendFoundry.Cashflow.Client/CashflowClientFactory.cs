﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

namespace LendFoundry.Cashflow.Client
{
    public class CashflowClientFactory : ICashflowClientFactory
    {
        public CashflowClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; set; }

        private string Endpoint { get; set; }

        private int Port { get; set; }

        public ICashflowService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new CashflowClient(client);
        }
    }
}