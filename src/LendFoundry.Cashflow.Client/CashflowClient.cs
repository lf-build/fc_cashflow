﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using RestSharp;
using System.Collections.Generic;
using System;
using System.Net;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Cashflow.Client
{
    public class CashflowClient : ICashflowService
    {
        public CashflowClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<string> GetBankName(string routingNumber)
        {
            var request = new RestRequest("bank/by/{routingNumber}", Method.GET);
            request.AddUrlSegment("routingNumber", routingNumber);
            return await Client.ExecuteAsync<string>(request);
        }

        //<<<<<<< HEAD
        //        public async Task<string> AddBankAccount(string entityType, string entityId, IBankAccountRequest BankAccount)
        //=======
        public async Task<IAddAccountResponse> AddBankAccount(string entityType, string entityId, IBankAccountRequest BankAccount)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/account", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(BankAccount);
            return await Client.ExecuteAsync<AddAccountResponse>(request);
        }

        public async Task AddCashFlowData(string entityType, string entityId, ICashflowRequest CashflowRequest)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/cashflow", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(CashflowRequest);
            await Client.ExecuteAsync<object>(request);
        }

        public Task<bool> ExtractAccountsAndTransactionsHandler(string entityType, string entityId, object request)
        {
            throw new NotImplementedException();
        }

        public async Task<string> AddBankTransaction(string entityType, string entityId, IBankTransactionRequest BankTransaction)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/transaction", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(BankTransaction);
            return await Client.ExecuteAsync<string>(request);
        }

        public async Task<bool> AddAccountPreference(string entityType, string entityId, IAccountPreferenceRequest accountPreference)
        {
//<<<<<<< HEAD
//            var requestAccount = new RestRequest("{entitytype}/{entityId}/add/account/preference", Method.POST);
//            requestAccount.AddUrlSegment("entitytype", entityType);
//            requestAccount.AddUrlSegment("entityId", entityId);
//            requestAccount.AddJsonBody(accountPreference);
//            return await Client.ExecuteAsync(requestAccount);
//=======
            var request = new RestRequest("{entitytype}/{entityId}/add/account/preference", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(accountPreference);
            return await Client.ExecuteAsync<bool>(request);
//>>>>>>> release / v2.0.0
        }

        public async Task<IBankAccount> GetBankAccountDetails(string entityType, string entityId, string accountId)
        {
            var request = new RestRequest("{entitytype}/{entityId}/{accountId}", Method.GET);
            request.AddUrlSegment("accountId", accountId);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<BankAccount>(request);
        }

        public async Task<IBankAccount> GetBankAccountDetailsById(string accountId)
        {
            var request = new RestRequest("{accountId}", Method.GET);
            request.AddUrlSegment("accountId", accountId);
            return await Client.ExecuteAsync<BankAccount>(request);
        }

        public Task<bool> CalculateManunalCashFlowEventHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public async Task<IAccountTypeResponse> GetAccountByType(string entityType, string entityId, IAccountTypeRequest request)
        {
            var requestAccount = new RestRequest("{entitytype}/{entityId}", Method.POST);
            requestAccount.AddUrlSegment("entitytype", entityType);
            requestAccount.AddUrlSegment("entityId", entityId);
            requestAccount.AddJsonBody(request);
            return await Client.ExecuteAsync<AccountTypeResponse>(requestAccount);
        }

//<<<<<<< HEAD
        public async Task AddBankLink(string bankId, string entityType, string entityId)
        {
            var requestAccount = new RestRequest("/link-bank/{bankId}/{entityType}/{entityId}", Method.POST);
            requestAccount.AddUrlSegment("bankId", bankId);
            requestAccount.AddUrlSegment("entityType", entityType);
            requestAccount.AddUrlSegment("entityId", entityId);
            await Client.ExecuteAsync(requestAccount);
        }

        public async Task<IBankLink> GetBankLink(string bankId)
        {
            var request = new RestRequest("/link-bank/{bankId}", Method.GET);
            request.AddUrlSegment("bankId", bankId);
            return await Client.ExecuteAsync<BankLink>(request);
        }

        public Task<bool> VerifyFinicityEvents(string entityType, string entityId, string eventName)
        {
            throw new NotImplementedException();
        }

        public async Task<IAccountTypeResponse> GetAllAccounts(string entityType, string entityId)
        {
            var requestAccount = new RestRequest("{entitytype}/{entityId}/accounts/all", Method.GET);
            requestAccount.AddUrlSegment("entitytype", entityType);
            requestAccount.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<AccountTypeResponse>(requestAccount);
        }

        public Task<bool> FinicityUserActionHandler(string entityType, string entityId, IEventRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<bool> InBoundHookInvokedHandler(string entityType, string entityId, IEventRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routingNumber"></param>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        public async Task<List<IBankAccount>> GetAccountByRoutingNumberAndAccountNumber(string routingNumber, string accountNumber)
        {
            var requestAccountAndRouting = new RestRequest("/routing-number/{routingNumber}/account-number/{accountNumber}", Method.GET);
            requestAccountAndRouting.AddUrlSegment("routingNumber", routingNumber);
            requestAccountAndRouting.AddUrlSegment("accountNumber", accountNumber);
            return new List<IBankAccount>(await Client.ExecuteAsync<List<BankAccount>>(requestAccountAndRouting));
        }

        public Task<bool> FinicityAccountHandler(string entityType, string entityId, IEventRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<bool> FinicityPartialTransactionHandler(string entityType, string entityId, IEventRequest request)
        {
            throw new NotImplementedException();
        }

        //public Task<List<ITransaction>> GetAccountTransaction(string entityType, string entityId, string accountId)
        //{
        //    throw new NotImplementedException();
        //}

        public Task<bool> FinicityAllTransactionReceivedHandler(string entityType, string entityId, IEventRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CashflowDocumentGeneratorHandler(string entityType, string entityId, IEventRequest request)
        {
            throw new NotImplementedException();
        }

        //=======
        public async Task<bool> CalculateRunningBalance(string entityType, string entityId, object data)
        {
            var request = new RestRequest("{entitytype}/{entityId}/calculate/runningbalance", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(data);
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<string> AddBankTransaction(string entityType, string entityId, IBulkTransactionRequest BankTransaction)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/transaction/bulk", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(BankTransaction);
            return await Client.ExecuteAsync<string>(request);
        }

        public async Task<ITransactionResponse> GetAccountTransaction(string entityType, string entityId, string accountId)
        {
            var request = new RestRequest("{entitytype}/{entityId}/transaction/{accountId}", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("accountId", accountId);
            return await Client.ExecuteAsync<TransactionResponse>(request);
        }

        public async Task<bool> CalculateRunningBalanceAll(string entityType, string entityId)
        {
            var request = new RestRequest("{entitytype}/{entityId}/calculate/runningbalance/all", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<IAddAccountBulkResponse> AddBankAccountBulk(string entityType, string entityId, IBulkAccountRequest BankAccount)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/account/bulk", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(BankAccount);
            return await Client.ExecuteAsync<AddAccountBulkResponse>(request);
        }

        public Task<bool> SaveAccountDetailsOcrolusHandler(string entityType, string entityId, IEventRequest request)
        {
            throw new NotImplementedException();
        }
        //>>>>>>> release / v2.0.0
    }
}