﻿namespace LendFoundry.Cashflow
{
    public interface IAccountPreference
    {
        string AccountID { get; set; }
        bool? IsCashflowAccount { get; set; }
        bool? IsFundingAccount { get; set; }
    }
}