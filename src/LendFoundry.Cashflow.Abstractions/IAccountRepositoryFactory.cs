﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Cashflow.Persistence
{
    public interface IAccountRepositoryFactory
    {
        IAccountRepository Create(ITokenReader reader);
    }
}