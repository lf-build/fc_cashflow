﻿namespace LendFoundry.Cashflow
{
    public interface IEventRequest
    {
        string DERuleName { get; set; }
        bool InitiateCashflow { get; set; }
        object data { get; set; }
    }
}