﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Cashflow
{
    public interface IBankAccount : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string RoutingNumber { get; set; }
        string BankName { get; set; }
        string ProviderAccountId { get; set; }
        string AccountNumber { get; set; }
        double CurrentBalance { get; set; }
        double AvailableBalance { get; set; }
        string AccountType { get; set; }
        string Source { get; set; }
        TimeBucket BalanceAsOfDate { get; set; }
        TimeBucket CreatedOn { get; set; }
        TimeBucket UpdatedOn { get; set; }
        string CreatedBy { get; set; }
        string UpdatedBy { get; set; }
        string NameOnAccount { get; set; }        
        bool IsCashflowAccount { get; set; }
        bool IsFundingAccount { get; set; }
        bool Refreshstatus { get; set; }
        TimeBucket LastRefreshDate { get; set; }
        string ProviderErrorCode { get; set; }
        string ProviderErrorDescription { get; set; }
        string AccessToken { get; set; }

        string OfficialAccountName { get; set; }
    }
}