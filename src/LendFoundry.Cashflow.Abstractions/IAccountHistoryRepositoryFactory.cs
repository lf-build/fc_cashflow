﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Cashflow.Persistence
{
    public interface IAccountHistoryRepositoryFactory
    {
        IAccountHistoryRepository Create(ITokenReader reader);
    }
}