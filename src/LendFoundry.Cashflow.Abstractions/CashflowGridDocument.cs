﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class CashflowGridDocument : ICashflowGridDocument
    {
        public string PDFReportName { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountType { get; set; }
        public List<CashflowGridReport> ReportList { get; set; }
    }

    public class GridReportTemplate
    {
        public string AccountHolderName { get; set; }
        public string AccountType { get; set; }
        public List<GridReport> ReportList { get; set; }
    }

    public class GridReport
    {
        public string ReportHeader { get; set; }
        public List<GridDataTemplate> GridData { get; set; }
    }

    public class GridDataTemplate
    {
        public string GridRowHeader { get; set; }
        public string Deposits { get; set; }
        public string DepositCount { get; set; }
        public string DepositAverage { get; set; }
        public string BeginningBalance { get; set; }
        public string EndingBalance { get; set; }
        public string IncDec { get; set; }
        public string ADB { get; set; }
        public string ADBPercentage { get; set; }
        public string NegativeDays { get; set; }
        public string NSF { get; set; }
    }

    public class CashflowGridReport : ICashflowGridReport
    {
        public string ReportHeader { get; set; }
        public List<GridData> GridData { get; set; }
    }

    public class ColumnValue : IColumnValue
    {
        public double Deposits { get; set; }
        public double DepositCount { get; set; }
        public double DepositAverage { get; set; }
        public double BeginningBalance { get; set; }
        public double EndingBalance { get; set; }
        public double IncDec { get; set; }
        public double ADB { get; set; }
        public double ADBPercentage { get; set; }
        public double NegativeDays { get; set; }
        public double NSF { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
    }

    public class GridData : IGridData
    {
        public string GridRowHeader { get; set; }
        public int GridRowOrder { get; set; }
        public ColumnValue ColumnValue { get; set; }
    }
}
