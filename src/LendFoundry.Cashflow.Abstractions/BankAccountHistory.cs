﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;

namespace LendFoundry.Cashflow
{
    public class BankAccountHistory : BankAccount, IBankAccountHistory
    {
        public string ReferenceId { get; set; }

    }
}