﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class Transaction : Aggregate, ITransaction
    {
        public Transaction() { }
        public Transaction(IBankTransactionRequest request)
        {
            AccountId = request.AccountId;
            ProviderAccountId = request.ProviderAccountId;
            Amount = request.Amount;
            TransactionDate = request.TransactionDate;
            Description = request.Description;
            Pending = request.Pending;
            CategoryId = request.CategoryId;
            Categories = request.Categories;
            Meta = request.Meta;
            Id = request.TransactionId;
            try
            {
                var TransactionDate_OffSet = Convert.ToDateTime(request.TransactionDate);
                TransactionOn = new TimeBucket(TransactionDate_OffSet);                
            }
            catch { }
        }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string AccountId { get; set; }
        public string ProviderAccountId { get; set; }
        public double Amount { get; set; }
        public string TransactionDate { get; set; }
        public string Description { get; set; }
        public string Pending { get; set; }
        public string CategoryId { get; set; }
        public IList<string> Categories { get; set; }
        public string Meta { get; set; }
        public TimeBucket CreatedOn { get; set; }
        public TimeBucket UpdatedOn { get; set; }
        public double RunningBalance { get; set; }
        public TimeBucket TransactionOn { get; set; }
    }
}