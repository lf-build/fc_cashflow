﻿namespace LendFoundry.Cashflow
{
    public class EventRequest : IEventRequest
    {
        public string DERuleName { get; set; }
        public bool InitiateCashflow { get; set; }
        public object data { get; set; }
    }
}