﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Cashflow.Persistence
{
    public interface ILinkRepositoryFactory
    {
        ILinkRepository Create(ITokenReader reader);
    }
}