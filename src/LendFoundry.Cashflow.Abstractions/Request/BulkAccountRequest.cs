﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class BulkAccountRequest : IBulkAccountRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<IBankAccount, BankAccount>))]
        public List<IBankAccount> bankAccount { get; set; }
    }
}