﻿namespace LendFoundry.Cashflow
{
    public interface IBankAccountRequest
    {
         string AccountNumber { get; set; } 
         string BankName { get; set; }
         string RoutingNumber { get; set;}
         double CurrentBalance { get; set; }
         double AvailableBalance { get; set; }
         string AccountType { get; set; }
         string Source { get; set; }
        string AccountId { get; set; }
        string BalanceAsOfDate { get; set; }
        string NameOnAccount { get; set; }
        string ProviderAccountId { get; set; }
        bool IsCashflowAccount { get; set; }
        bool IsFundingAccount { get; set; }
        string AccessToken { get; set; }
        string OfficialAccountName { get; set; }
    }
}