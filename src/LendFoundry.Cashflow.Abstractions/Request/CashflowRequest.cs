﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Cashflow
{
    public class CashflowRequest: ICashflowRequest
    {
        public CashflowRequest() { }
        public CashflowRequest(ICashflowRequest request)
        {
            AccountHeader = request.AccountHeader;
            AccountID = request.AccountID;
            AccountNumber = request.AccountNumber;
            InstitutionName = request.InstitutionName;
            AccountType = request.AccountType;
            CashFlow = new CashflowDetails(request.CashFlow);
            HasSubReports = request.HasSubReports;
            Source = request.Source;
        }
        public string AccountHeader { get; set; }
        public string AccountID { get; set; }
        public string AccountNumber { get; set; }
        public string InstitutionName { get; set; }
        public string AccountType { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICashflowDetails, CashflowDetails>))]
        public ICashflowDetails CashFlow { get; set; }
        public bool HasSubReports { get; set; }
        public string Source { get; set; }
    }
}
