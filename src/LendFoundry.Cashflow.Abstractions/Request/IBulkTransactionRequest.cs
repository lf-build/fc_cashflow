﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public interface IBulkTransactionRequest
    {
        List<ITransaction> bankTransaction { get; set; }
    }
}