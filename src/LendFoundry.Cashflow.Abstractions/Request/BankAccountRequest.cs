﻿namespace LendFoundry.Cashflow
{
    public class BankAccountRequest :  IBankAccountRequest
    {
        public string AccountNumber { get; set; }
        public string BankName { get; set; }
        public string RoutingNumber { get; set; }
        public double CurrentBalance { get; set; }
        public double AvailableBalance { get; set; }
        public string AccountType { get; set; }
        public string Source { get; set; }
        public string AccountId { get; set; }
        public string BalanceAsOfDate { get; set; }
        public string NameOnAccount { get; set; }
        public string ProviderAccountId { get; set; }
        public bool IsCashflowAccount { get; set; }
        public bool IsFundingAccount { get; set; }
        public string AccessToken { get; set; }
        public string OfficialAccountName { get; set; }
    }
}