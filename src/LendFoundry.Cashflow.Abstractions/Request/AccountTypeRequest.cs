﻿namespace LendFoundry.Cashflow
{
    public class AccountTypeRequest : IAccountTypeRequest
    {
        public bool? IsFundingAccount { get; set; }
        public bool? IsCashflowAccount { get; set; }
    }
}