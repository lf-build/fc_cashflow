﻿namespace LendFoundry.Cashflow
{
    public interface ICSVRequest
    {
        string accountNumber { get; set; }
        string instituteName { get; set; }
        string accountType { get; set; }
        string fileContent { get; set; }
        string fileName { get; set; }
        string routingNumber { get; set; }
    }
}