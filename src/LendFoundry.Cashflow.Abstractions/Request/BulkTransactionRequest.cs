﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class BulkTransactionRequest : IBulkTransactionRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<ITransaction, Transaction>))]
        public List<ITransaction> bankTransaction { get; set; }
    }
}