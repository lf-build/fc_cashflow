﻿namespace LendFoundry.Cashflow
{
    public class CSVRequest : ICSVRequest
    {
        public string accountNumber { get; set; }
        public string instituteName { get; set; }
        public string accountType { get; set; }
        public string fileContent { get; set; }
        public string fileName { get; set; }
        public string routingNumber { get; set; }
    }
}