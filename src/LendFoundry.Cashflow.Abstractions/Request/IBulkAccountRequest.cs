﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public interface IBulkAccountRequest
    {
        List<IBankAccount> bankAccount { get; set; }
    }
}