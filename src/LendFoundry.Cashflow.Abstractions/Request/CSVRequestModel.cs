﻿namespace LendFoundry.Cashflow
{
    public class CSVRequestModel
    {
        public object Request { get; set; }
        public object Response { get; set; }
    }
}