﻿namespace LendFoundry.Cashflow
{
    public interface IAccountTypeRequest
    {
        bool? IsFundingAccount { get; set; }
        bool? IsCashflowAccount { get; set; }
    }
}