﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Cashflow
{
    public interface IBankAccountHistory : IAggregate
    {
        string ReferenceId { get; set; }
        
    }
}