﻿namespace LendFoundry.Cashflow
{
    public interface ILinks
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
    }
}