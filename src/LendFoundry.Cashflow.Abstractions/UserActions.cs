﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow
{
    public class UserActions
    {
        public UserActions() { }
        public List<UserActionDetail> CustomerDetails { get; set; }
    }
}
