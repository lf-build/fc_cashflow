﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow
{
    public interface ITransactionRepository
    {         
        Task<string> AddTransaction(ITransaction request);
        Task<List<ITransaction>> GetAllBankTransaction(string entityType, string entityId,string accountId);
        Task<string> AddTransaction(List<ITransaction> request);
        Task<int> GetTransactionCount(string entityType, string entityId, string accountId);
    }
}