﻿using System.Collections.Generic;
using System.IO;

namespace LendFoundry.Cashflow
{
    public interface IPdfMergerService
    {
        byte[] MergePdf(IEnumerable<Stream> streams);
    }
}
