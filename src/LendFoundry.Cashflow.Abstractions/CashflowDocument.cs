﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class CashflowDocument : ICashflowDocument
    {
        public string Logo { get; set; }
        public CashflowAccountSection CashflowAccountSection { get; set; }
        public List<CashflowTransactionListSection> TransactionList { get; set; }
        public List<CashflowCategorySummarySection> CashflowCategorySummary { get; set; }
        public TransactionSummarySection TransactionSummary { get; set; }
        public List<RecurringTransactionSection> RecurringTransaction { get; set; }
        public MonthlyCashFlowSummarySection MonthlySummary { get; set; }
    }

    public class TransactionSummarySection
    {
        public string AsOfDate { get; set; }
        public string AverageDailyBalance { get; set; }
        public string AverageDeposit { get; set; }
        public string AverageWithdrawal { get; set; }
        public string ChangeInDepositVolume { get; set; }
        public string CountOfMonthlyStatement { get; set; }
        public string EndDate { get; set; }
        public string NumberOfNegativeBalance { get; set; }
        public string StartDate { get; set; }
        public string TotalCredits { get; set; }
        public string TotalDebits { get; set; }
        public string AvailableBalance { get; set; }
        public string CurrentBalance { get; set; }
        public string AverageBalanceLastMonth { get; set; }

        public string CVOfDailyBalance { get; set; }
        public string CVOfDailyDeposit { get; set; }
        public string MaxDaysBelow100Count { get; set; }
        public string MedianDailyBalance { get; set; }
        public string MedianMonthlyIncome { get; set; }

        public string TotalCreditsCount { get; set; }
        public string TotalDebitsCount { get; set; }
        public string AnnualCalculatedRevenue { get; set; }
        public string BrokerAGS { get; set; }
        public string CAPAGS { get; set; }
    }

    public class CashflowTransactionListSection
    {
        public string Date { get; set; }
        public string Amount { get; set; }
        public string Description { get; set; }
        public string TransactionType { get; set; }
        public string RunningBalance { get; set; }
        public string Credit { get; set; }
        public string Debit { get; set; }
    }

    public class CashflowAccountSection
    {
        public string LoanNumber { get; set; }
        public string InstitutionName { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountType { get; set; }
        public string Email { get; set; }
        public string AccountNumber { get; set; }

        public string MonthPeriod { get; set; }
    }

    public class RecurringTransactionSection
    {
        public string Account { get; set; }
        public string Amount { get; set; }
        public string RoudingAmount { get; set; }
        public string Name { get; set; }
        public string Min { get; set; }
        public string Max { get; set; }
        public string MaxMinDiff { get; set; }
        public string TotalTransactions { get; set; }
        public bool IsRecurring { get; set; }
        public string Status { get; set; }
        public string ConfidenceLevel { get; set; }
        public List<CashflowTransactionListSection> Transactions { get; set; }
    }

    public class CashflowCategorySummarySection
    {
        public string TransactionCount { get; set; }
        public string CategoryName { get; set; }
        public string TransactionTotal { get; set; }
        public string CategoryId { get; set; }
        public string CustomCategoryId { get; set; }
        public string LastMonthTransactionCount { get; set; }
        public string LastMonthTransactionTotal { get; set; }
    }

    public class MonthlyCashFlowSummarySection
    {
        public string AverageDailyBalance { get; set; }
        public string AverageDailyBalancePercent { get; set; }
        public string AverageDeposit { get; set; }
        public string BeginingBalance { get; set; }
        public string GridRowHeader { get; set; }
        public string CustomAttributes { get; set; }
        public string DateOfMonthlyCycle { get; set; }
        public string DepositCount { get; set; }
        public string EndingBalance { get; set; }
        public string EndTransactionDate { get; set; }
        public string FirstTransactionDate { get; set; }
        public string IncDecBalance { get; set; }
        public string LoanPaymentAmount { get; set; }
        public string MaxDepositAmount { get; set; }
        public string MaxWithdrawalAmount { get; set; }
        public string MinDepositAmount { get; set; }
        public string MinWithdrawalAmount { get; set; }
        public int MonthInNumber { get; set; }
        public string Name { get; set; }
        public string NSFAmount { get; set; }
        public string NumberOfLoanPayment { get; set; }
        public string NumberOfNegativeBalance { get; set; }
        public string NumberOfNSF { get; set; }
        public string NumberOfPayroll { get; set; }
        public string NumberOfSpecialCategoryDeposit { get; set; }
        public string PayrollAmount { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public string TotalDepositAmount { get; set; }
        public string TotalWithdrawalAmount { get; set; }
        public string WithdrawalCount { get; set; }
        public int Year { get; set; }
    }
}