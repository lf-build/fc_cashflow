﻿using System.Collections.Generic;


namespace LendFoundry.Cashflow
{
    public class RecurringTransaction
    {
        public string Account { get; set; }
        public double Amount { get; set; }
        public double RoudingAmount { get; set; }
        public string Name { get; set; }
        public string Min { get; set; }
        public string Max { get; set; }
        public string MaxMinDiff { get; set; }
        public string TotalTransactions { get; set; }
        public bool IsRecurring { get; set; }
        public string Status { get; set; }
        public string ConfidenceLevel { get; set; }
        public List<ReportTransaction> Transactions { get; set; }
    }
}
