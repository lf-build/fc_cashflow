﻿namespace LendFoundry.Cashflow
{
    public class MonthlyCashFlow
    {
        public double? AverageDailyBalance { get; set; }
        public int MonthInNumber { get; set; }
        public double? AverageDailyBalancePercent { get; set; }
        public double? AverageDeposit { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public double? IncDecBalance { get; set; }
        public string GridRowHeader { get; set; }
        public double? BeginingBalance { get; set; }
        public string CustomAttributes { get; set; }
        public int DepositCount { get; set; }
        public double? EndingBalance { get; set; }
        public string FirstTransactionDate { get; set; }
        public string EndTransactionDate { get; set; }
        public int LoanPaymentAmount { get; set; }
        public double? MaxDepositAmount { get; set; }
        public double? MaxWithdrawalAmount { get; set; }
        public double? MinDepositAmount { get; set; }
        public double? MinWithdrawalAmount { get; set; }
        public int NSFAmount { get; set; }
        public string Name { get; set; }
        public int NumberOfLoanPayment { get; set; }
        public int NumberOfNSF { get; set; }
        public int NumberOfNegativeBalance { get; set; }
        public int NumberOfPayroll { get; set; }
        public double PayrollAmount { get; set; }
        public double TotalDepositAmount { get; set; }
        public double TotalWithdrawalAmount { get; set; }
        public int WithdrawalCount { get; set; }
        public int Year { get; set; }
        public string PDFReportName { get; set; }
        public string NumberOfSpecialCategoryDeposit { get; set; }
        public string DateOfMonthlyCycle { get; set; }
    }
}
