﻿namespace LendFoundry.Cashflow
{
    public class CashflowTemplate
    {
        public string AccountNumber { get; set; }
        public string AccountID { get; set; }
        public string InstitutionName { get; set; }
        public string AccountType { get; set; }
        public CashFlow CashFlow { get; set; }
        public string MonthPeriod { get; set; }
    } 
}
