﻿namespace LendFoundry.Cashflow
{
    public class TransactionSummary
    {
        public double AverageDailyBalance { get; set; }
        public double AverageDeposit { get; set; }
        public double AverageWithdrawal { get; set; }
        public int ChangeInDepositVolume { get; set; }
        public int CountOfMonthlyStatement { get; set; }
        public string EndDate { get; set; }
        public int LoanPaymentAmount { get; set; }
        public int NSFAmount { get; set; }
        public int NumberOfLoanPayment { get; set; }
        public int NumberOfNSF { get; set; }
        public int NumberOfNegativeBalance { get; set; }
        public int NumberOfPayroll { get; set; }
        public double PayrollAmount { get; set; }
        public string StartDate { get; set; }
        public int TotalCreditsCount { get; set; }
        public int TotalDebitsCount { get; set; }
        public double TotalCredits { get; set; }
        public double TotalDebits { get; set; }
        public double AvailableBalance { get; set; }
        public double CurrentBalance { get; set; }
        public double AverageBalanceLastMonth { get; set; }
        public double CVOfDailyBalance { get; set; }
        public double CVOfDailyDeposit { get; set; }
        public int MaxDaysBelow100Count { get; set; }
        public double MedianDailyBalance { get; set; }
        public double MedianMonthlyIncome { get; set; }
        public string PDFReportName { get; set; }
        public double AnnualCalculatedRevenue { get; set; }
        public double BrokerAGS { get; set; }
        public double CAPAGS { get; set; }
        public string BrokerAGSText { get; set; }
    }
}
