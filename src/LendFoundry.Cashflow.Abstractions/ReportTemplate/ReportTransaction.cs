﻿using System.Collections.Generic;


namespace LendFoundry.Cashflow
{
    public class ReportTransaction
    {
        public string Date { get; set; }
        public double Amount { get; set; }
        public string Description { get; set; }
        public string TransactionType { get; set; }
        public double RunningBalance { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }

        public double Credit { get; set; }
        public double Debit { get; set; }
    }
}
