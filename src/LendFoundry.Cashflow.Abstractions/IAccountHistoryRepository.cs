﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.Persistence
{
    public interface IAccountHistoryRepository
    {
        Task<string> UpdateAccountHistory(IBankAccount request);
    }
}