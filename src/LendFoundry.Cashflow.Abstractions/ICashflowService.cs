﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow
{
    public interface ICashflowService
    {
        Task<string> GetBankName(string routingNumber);

        // Task<string> AddBankAccount(string entityType, string entityId, IBankAccountRequest bankAccount);
        Task<string> AddBankTransaction(string entityType, string entityId, IBankTransactionRequest bankTransaction);
        Task AddCashFlowData(string entityType, string entityId, ICashflowRequest cashflowRequest);
        Task<bool> AddAccountPreference(string entityType, string entityId, IAccountPreferenceRequest accountPreference);
        Task<IBankAccount> GetBankAccountDetails(string entityType, string entityId, string accountId);
        Task<IBankAccount> GetBankAccountDetailsById(string accountId);
        Task<bool> ExtractAccountsAndTransactionsHandler(string entityType, string entityId, object request);
        Task<bool> CalculateManunalCashFlowEventHandler(string entityType, string entityId, object data);
        // Task<IAccountTypeResponse> GetAccountByType(string entityType, string entityId, IAccountTypeRequest request);
        Task<bool> VerifyFinicityEvents(string entityType, string entityId, string eventName);
        Task<IAccountTypeResponse> GetAllAccounts(string entityType, string entityId);
        Task AddBankLink(string bankId, string entityType, string entityId);
        Task<IBankLink> GetBankLink(string bankId);
        Task<bool> FinicityUserActionHandler(string entityType, string entityId, IEventRequest request);
        Task<bool> InBoundHookInvokedHandler(string entityType, string entityId, IEventRequest request);
        Task<bool> FinicityAccountHandler(string entityType, string entityId, IEventRequest request);
        Task<bool> FinicityPartialTransactionHandler(string entityType, string entityId, IEventRequest request);
        Task<bool> FinicityAllTransactionReceivedHandler(string entityType, string entityId, IEventRequest request);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="routingNumber"></param>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        Task<List<IBankAccount>> GetAccountByRoutingNumberAndAccountNumber(string routingNumber, string accountNumber);
        // Task<List<ITransaction>> GetAccountTransaction(string entityType, string entityId, string accountId);

        Task<IAddAccountResponse> AddBankAccount(string entityType, string entityId, IBankAccountRequest BankAccount);
        //Task<bool> ExtractAccountsAndTransactionsHandler(string entityType, string entityId, object request);
        //Task<bool> CalculateManunalCashFlowEventHandler(string entityType, string entityId, object request);
        Task<IAccountTypeResponse> GetAccountByType(string entityType, string entityId, IAccountTypeRequest request);
        Task<bool> CalculateRunningBalance(string entityType, string entityId, object data);
        Task<string> AddBankTransaction(string entityType, string entityId, IBulkTransactionRequest BankTransaction);

        Task<ITransactionResponse> GetAccountTransaction(string entityType, string entityId, string accountId);
        Task<bool> CalculateRunningBalanceAll(string entityType, string entityId);
        Task<IAddAccountBulkResponse> AddBankAccountBulk(string entityType, string entityId, IBulkAccountRequest BankAccount);
        Task<bool> SaveAccountDetailsOcrolusHandler(string entityType, string entityId, IEventRequest request);
    }
}