﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public interface IAccountsAndTransactions
    {
        List<BankAccount> Accounts { get; set; }
        List<Transaction> Transactions { get; set; }

    }
}