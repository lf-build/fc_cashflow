﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public interface IBankLink : IAggregate
    {
        string BankId { get; set; }
        List<ILinks> Links { get; set; }
    }
}