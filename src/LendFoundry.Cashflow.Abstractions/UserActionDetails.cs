﻿using LendFoundry.Foundation.Date;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow
{
    public class UserActionDetail
    {

        public string ProviderAccountId { get; set; }
        public string AccountNumber { get; set; }
        public double CurrentBalance { get; set; }
        public double AvailableBalance { get; set; }
        public string AccountType { get; set; }
        public string Source { get; set; }
        public string Institutionid { get; set; }
        public bool IsCashflowAccount { get; set; }
        public bool IsFundingAccount { get; set; }
        public DateTime? BalanceDate { get; set; }
        public string AggregationStatusCode { get; set; }
        public string AggregationStatusDescription { get; set; }

        public DateTime? AggregationAttemptDate { get; set; }
        public string CustomerId { get; set; }
        public string NameOnAccount { get; set; }

        public string BankName { get; set; }



    }
}
