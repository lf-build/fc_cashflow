﻿namespace LendFoundry.Cashflow
{
    public class ACHResponse 
    {
        public string RoutingNumber { get; set; }
        public string RealAccountNumber { get; set; }
        public string AccountId { get; set; }  
    }
}