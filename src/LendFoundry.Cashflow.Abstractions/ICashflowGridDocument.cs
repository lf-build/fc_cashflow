﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow
{
    public interface ICashflowGridDocument
    {
        string PDFReportName { get; set; }
        string AccountHolderName { get; set; }
        string AccountType { get; set; }
        List<CashflowGridReport> ReportList { get; set; }        
    }

    public interface ICashflowGridReport
    {
        string ReportHeader { get; set; }
        List<GridData> GridData { get; set; }
    }

    public interface IGridData
    {
        string GridRowHeader { get; set; }
        int GridRowOrder { get; set; }
        ColumnValue ColumnValue { get; set; }       
    }

    public interface IColumnValue
    {
        double Deposits { get; set; }
        double DepositCount { get; set; }
        double DepositAverage { get; set; }
        double BeginningBalance { get; set; }
        double EndingBalance { get; set; }
        double IncDec { get; set; }
        double ADB { get; set; }
        double ADBPercentage { get; set; }
        double NegativeDays { get; set; }
        double NSF { get; set; }
        string Name { get; set; }
        string Year { get; set; }
    }
}
