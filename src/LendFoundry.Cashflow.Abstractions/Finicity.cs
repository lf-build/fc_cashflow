﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class FinicityAccount
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public double Balance { get; set; }
        public string Type { get; set; }
        public int AggregationStatusCode { get; set; }
        public string Status { get; set; }
        public string CustomerId { get; set; }
        public string InstitutionId { get; set; }
        public int BalanceDate { get; set; }
        public int AggregationSuccessDate { get; set; }
        public int AggregationAttemptDate { get; set; }
        public int CreatedDate { get; set; }
        public int LastUpdatedDate { get; set; }
        public string Currency { get; set; }
        public int LastTransactionDate { get; set; }
        public int InstitutionLoginId { get; set; }
        public int DisplayPosition { get; set; }
    }

    public class Categorization
    {
        public string normalizedPayeeName { get; set; }
        public string sic { get; set; }
        public string category { get; set; }
        public string scheduleC { get; set; }
    }

    public class FinicityTransaction
    {
        public int accountId { get; set; }
        public double amount { get; set; }
        public object bonusAmount { get; set; }
        public object checkNum { get; set; }
        public int createdDate { get; set; }
        public int customerId { get; set; }
        public string description { get; set; }
        public object escrowAmount { get; set; }
        public object feeAmount { get; set; }
        public object id { get; set; }
        public object interestAmount { get; set; }
        public string memo { get; set; }
        public int postedDate { get; set; }
        public object principalAmount { get; set; }
        public string status { get; set; }
        public int transactionDate { get; set; }
        public object type { get; set; }
        public object unitQuantity { get; set; }
        public object unitValue { get; set; }
        public Categorization categorization { get; set; }
    }

    public class FinicityTransactionResponse
    {
        public string moreAvailable { get; set; }
        public int totalRecords { get; set; }
        public int displayingRecords { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string sort { get; set; }
        public List<FinicityTransaction> transactions { get; set; }
    }
}