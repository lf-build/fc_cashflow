﻿namespace LendFoundry.Cashflow
{
    public class Links : ILinks
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
    }
}
