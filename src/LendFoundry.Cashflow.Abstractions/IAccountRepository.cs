﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.Persistence
{
    public interface IAccountRepository
    {         
        Task<string> AddBankAccount(IBankAccount request);
        Task<bool> UpdateAccountPreference(IAccountPreference request);
        Task<IBankAccount> GetAccountDetails(string entityType, string entityId,string id);
        Task<IBankAccount> GetAccountDetailsById(string id);
        Task<List<IBankAccount>> GetAllBankAccounts(string entityType, string entityId);
        Task<IBankAccount> GetCashflowAccount(string entityType, string entityId);
        Task<IBankAccount> GetFundingAccount(string entityType, string entityId);
        Task<bool> ResetAccountPreference(string entityType, string entityId, AccountType accountType);
        Task<IBankAccount> GetAccountByProvideAccountId(string entityType, string entityId, string provideAccountId);
        Task<IBankAccount> GetFinicityAccountByProviderAccountId(string provideAccountId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="routingNumber"></param>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        Task<List<IBankAccount>> GetAccountByRoutingNumberAndAccountNumber(string routingNumber, string accountNumber);

        Task<List<IBankAccount>> AddBankAccount(string entityType, string entityId, List<IBankAccount> request);

    }
}