﻿namespace LendFoundry.Cashflow
{
    public class ReportAttribute : ICashflowReportAttribute
    {
        public bool IsActive { get; set; }
        public bool Transactions { get; set; }
        public bool RecurringTransactions { get; set; }
    }
}