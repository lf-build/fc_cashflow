﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public interface ICashflowConfiguration
    {
         string BanklookupUrl { get; set; }
         string Source { get; set; }
        List<EventConfiguration> events { get; set; }
        CashflowReportConfig CashflowReportConfig { get; set; }
        string DataAttributeStoreName { get; set; }  
        Dictionary<string,string> RuleName { get; set; }
    }
}