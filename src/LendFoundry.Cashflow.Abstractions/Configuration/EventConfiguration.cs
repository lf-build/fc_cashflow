﻿namespace LendFoundry.Cashflow
{
    public class EventConfiguration : IEventConfiguration
    {
        public string EntityId { get; set; }
        public string Response { get; set; }
        public string Request { get; set; }
        public string EntityType { get; set; }
        public string Name { get; set; }
        public string MethodToExecute { get; set; }
        public string CompletionEventName { get; set; }
        public string DERuleName { get; set; }
        public bool InitiateCashflow { get; set; }
    }
}