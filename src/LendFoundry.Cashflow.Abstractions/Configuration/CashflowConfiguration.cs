﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class CashflowConfiguration: ICashflowConfiguration
    { 
        public string BanklookupUrl { get; set; }
        public string Source { get; set; }
        public List<EventConfiguration> events { get; set; }
        public CashflowReportConfig CashflowReportConfig { get; set; }
        public string DataAttributeStoreName { get; set; }   
        public Dictionary<string, string> RuleName { get; set; }
    }
}