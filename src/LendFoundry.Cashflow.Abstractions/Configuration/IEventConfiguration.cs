﻿namespace LendFoundry.Cashflow
{
    public interface IEventConfiguration
    {
        string EntityId { get; set; }
        string Response { get; set; }
        string Request { get; set; }
        string EntityType { get; set; }
        string Name { get; set; }
        string MethodToExecute { get; set; }
        string CompletionEventName { get; set; }
        string DERuleName { get; set; }
        bool InitiateCashflow { get; set; }
    }
}