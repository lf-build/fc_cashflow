﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Cashflow
{
    public interface ITransactionRepositoryFactory
    {
        ITransactionRepository Create(ITokenReader reader);
    }
}