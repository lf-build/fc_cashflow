﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class BankLink : Aggregate, IBankLink
    {
        public string BankId { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILinks, Links>))]
        public List<ILinks> Links { get; set; }
    }
}
