﻿namespace LendFoundry.Cashflow
{
    public enum AccountType
    {
        Funding=1,
        Cashflow=2
    }
}