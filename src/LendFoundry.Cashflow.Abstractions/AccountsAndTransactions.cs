﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class AccountsAndTransactions : IAccountsAndTransactions
    {
        public AccountsAndTransactions() { }

        public List<BankAccount> Accounts { get; set; }
        public List<Transaction> Transactions { get; set; }

    }
}