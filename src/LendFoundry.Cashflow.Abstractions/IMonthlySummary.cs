﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public interface IMonthlySummary
    {
        string Name { get; set; }
        Dictionary<string, string> MonthlyAccountsSummary { get; set; }
        string Year { get; set; }
    }
}