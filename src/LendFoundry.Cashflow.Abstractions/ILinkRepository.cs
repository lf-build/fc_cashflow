﻿using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.Persistence
{
    public interface ILinkRepository : IRepository<IBankLink>
    {
        Task<IBankLink> GetBankLinks(string bankId);
    }
}