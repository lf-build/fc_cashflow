﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace LendFoundry.Cashflow
{
    public static class XmlDeserialization
    {

        public static T Deserialize<T>(string xmlContent)
        {
            try
            {
                var xmlSerializer = new XmlSerializer(typeof(T));

                using (var sr = new StringReader(xmlContent))
                    return (T)xmlSerializer.Deserialize(sr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
