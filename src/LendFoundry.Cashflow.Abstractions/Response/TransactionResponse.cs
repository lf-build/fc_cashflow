﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class TransactionResponse : ITransactionResponse
    {
        [JsonConverter(typeof(InterfaceListConverter<ITransaction, Transaction>))]
        public List<ITransaction> transaction { get; set; }
    }
}