﻿
namespace LendFoundry.Cashflow
{
    public interface IAddAccountResponse
    {
        string AccountId { get; set; }
    }
}