﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public interface IAccountTypeResponse
    {
        List<IBankAccount> accounts { get; set; }
    }
}