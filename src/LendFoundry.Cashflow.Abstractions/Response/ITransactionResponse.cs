﻿using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public interface ITransactionResponse
    {
        List<ITransaction> transaction { get; set; }
    }
}