﻿
namespace LendFoundry.Cashflow
{
    public class AddAccountResponse : IAddAccountResponse
    {
        public string AccountId { get; set; }
    }
}