﻿namespace LendFoundry.Cashflow
{
    public interface IAccountBulkResponse
    {
        string AccountId { get; set; }
        string ProviderAccountId { get; set; }
    }
}