﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class AccountTypeResponse : IAccountTypeResponse
    {
        [JsonConverter(typeof(InterfaceListConverter<IBankAccount, BankAccount>))]
        public List<IBankAccount> accounts { get; set; }
    }
}