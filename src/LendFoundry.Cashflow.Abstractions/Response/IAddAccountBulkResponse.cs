﻿
using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public interface IAddAccountBulkResponse
    {        
        List<IAccountBulkResponse> bankAccounts { get; set; }
    }
   
}