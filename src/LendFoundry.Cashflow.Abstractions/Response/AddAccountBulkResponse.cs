﻿
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Cashflow
{
    public class AddAccountBulkResponse : IAddAccountBulkResponse
    {
        [JsonConverter(typeof(InterfaceListConverter<IAccountBulkResponse, AccountBulkResponse>))]
        public List<IAccountBulkResponse> bankAccounts { get; set; }
    }
}