﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver.Linq;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.Persistence
{
    public class LinkRepository : MongoRepository<IBankLink, BankLink>, ILinkRepository
    {
        static LinkRepository()
        {
            BsonClassMap.RegisterClassMap<BankLink>(map =>
            {
                map.AutoMap();
                var type = typeof(BankLink);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Links>(map =>
            {
                map.AutoMap();
                var type = typeof(Links);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public LinkRepository(ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "BankLinks")
        {
        }

        public async Task<IBankLink> GetBankLinks(string bankId)
        {
            return await Query.Where(b => b.TenantId == TenantService.Current.Id && b.BankId == bankId).FirstOrDefaultAsync();
        }
    }
}