﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Cashflow.Persistence
{
    public class LinkRepositoryFactory : ILinkRepositoryFactory
    {
        public LinkRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ILinkRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();

            return new LinkRepository(tenantService, mongoConfiguration);
        }       
    }
}