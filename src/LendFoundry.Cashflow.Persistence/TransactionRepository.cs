﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.Persistence
{
    public class TransactionRepository : MongoRepository<ITransaction, Transaction>, ITransactionRepository
    {
        static TransactionRepository()
        {
            BsonClassMap.RegisterClassMap<Transaction>(map =>
            {
                map.AutoMap();
                var type = typeof(Transaction);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public TransactionRepository(ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "BankTransaction")
        {
            CreateIndexIfNotExists("BankTransaction_BankTransactionId", Builders<ITransaction>.IndexKeys.Ascending(i => i.EntityId));
        }

        public async Task<string> AddTransaction(ITransaction request)
        {
            if (!string.IsNullOrEmpty(request.Id))
            {
                var Transaction = await Get(request.Id);
                if (Transaction != null)
                {
                    Transaction.AccountId = request.AccountId;
                    Transaction.Amount = request.Amount;
                    Transaction.Categories = request.Categories;
                    Transaction.CategoryId = request.CategoryId;
                    Transaction.Description = request.Description;
                    Transaction.Meta = request.Meta;
                    Transaction.Pending = request.Pending;
                    Transaction.ProviderAccountId = request.ProviderAccountId;
                    Transaction.TransactionDate = request.TransactionDate;
                    Transaction.UpdatedOn = request.UpdatedOn;
                    Update(request);
                }
            }
            else
            {
                Add(request);
                return request.Id;
            }

            return request.Id;
        }

        public async Task<List<ITransaction>> GetAllBankTransaction(string entityType, string entityId, string accountId)
        {
            var record = Query.Where(i => i.EntityId == entityId && i.EntityType == entityType && i.AccountId==accountId);
            return record?.ToList();
        }

        public async Task<int> GetTransactionCount(string entityType, string entityId, string accountId)
        {
            var record = Query.Count(i => i.EntityId == entityId && i.EntityType == entityType && i.AccountId == accountId);
            return record;
        }

        public async Task<string> AddTransaction(List<ITransaction> request)
        {
            var tenantId = TenantService.Current.Id;
            request.ForEach(t => t.TenantId = tenantId);
            Collection.InsertMany(request);
            return "Success";
        }      
    }
}