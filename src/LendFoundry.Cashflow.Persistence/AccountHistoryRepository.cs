﻿
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;

namespace LendFoundry.Cashflow.Persistence
{
    public class AccountHistoryRepository : MongoRepository<IBankAccountHistory, BankAccountHistory>, IAccountHistoryRepository
    {
        static AccountHistoryRepository()
        {
            BsonClassMap.RegisterClassMap<BankAccountHistory>(map =>
            {
                map.AutoMap();
                var type = typeof(BankAccountHistory);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
           
        }

        public AccountHistoryRepository(ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "BankAccountHistory")
        {
            //  CreateIndexIfNotExists("BankAccount_BankAccountId", Builders<IBankAccount>.IndexKeys.Ascending(i => i.EntityId));
        }

        public async Task<string> UpdateAccountHistory(IBankAccount request)
        {
            if (!string.IsNullOrEmpty(request.Id))
            {
                var accounthistory = new BankAccountHistory();
               
                if (request != null)
                {
                    accounthistory.ReferenceId = request.Id;

                    accounthistory.EntityId = request.EntityId;
                    accounthistory.EntityType = request.EntityType;
                    accounthistory.RoutingNumber = request.RoutingNumber;
                    accounthistory.BankName = request.BankName;
                    accounthistory.ProviderAccountId = request.ProviderAccountId;
                    accounthistory.AccountNumber = request.AccountNumber;
                    accounthistory.CurrentBalance = request.CurrentBalance;
                    accounthistory.AvailableBalance = request.AvailableBalance;
                    accounthistory.AccountType = request.AccountType;
                    accounthistory.Source = request.Source;
                    accounthistory.BalanceAsOfDate = request.BalanceAsOfDate;
                    accounthistory.NameOnAccount = request.NameOnAccount;
                    accounthistory.IsCashflowAccount = request.IsCashflowAccount;
                    accounthistory.IsFundingAccount = request.IsFundingAccount;
                    accounthistory.CreatedOn = request.CreatedOn;
                    accounthistory.CreatedBy = request.CreatedBy;
                    accounthistory.UpdatedOn = request.UpdatedOn;
                    accounthistory.UpdatedBy = request.UpdatedBy;
                    accounthistory.AccessToken = request.AccessToken;

                    Add(accounthistory);
                    return request.Id;
                }
            }
           
            return request.Id;
        }
        
    }
}