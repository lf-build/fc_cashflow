﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.IO;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.Api.Controllers
{
    [Route("/")]
    public class CashflowController : ExtendedController
    {
        public CashflowController(ICashflowService service, ILogger logger) : base(logger)
        {
            Service = service;
        }

        private ICashflowService Service { get; }

        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        [HttpPost("{entitytype}/{entityId}/add/account")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Cashflow.IAddAccountResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddBankAccount(string entityType, string entityId, [FromBody]BankAccountRequest bankAccount)
        {
            return await ExecuteAsync(async () =>
           {
               return Ok(await Service.AddBankAccount(entityType, entityId, bankAccount));
           });
        }

        [HttpPost("{entitytype}/{entityId}/add/cashflow")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddCashFlowData(string entityType, string entityId, [FromBody]CashflowRequest cashflowRequest)
        {
            return await ExecuteAsync(
             async () =>
                 {
                     await Service.AddCashFlowData(entityType, entityId, cashflowRequest);
                     return Ok();
                 });
        }

        [HttpGet("/bank/by/{routingNumber}")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetBankName(string routingNumber)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetBankName(routingNumber)));
            });
        }

        [HttpPost("{entitytype}/{entityId}/add/account/preference")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddAccountPreference(string entityType, string entityId, [FromBody] AccountPreferenceRequest accountPreference)
        {
            return Ok(await Task.Run(() => Service.AddAccountPreference(entityType, entityId, accountPreference)));
        }

        [HttpGet("{entitytype}/{entityId}/{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Cashflow.IBankAccount), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetBankAccountDetails(string entityType, string entityId, string accountId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetBankAccountDetails(entityType, entityId, accountId)));
            });
        }

        [HttpGet("{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Cashflow.IBankAccount), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetBankAccountDetailsById(string accountId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetBankAccountDetailsById(accountId)));
            });
        }

        [HttpPost("{entitytype}/{entityId}")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Cashflow.IAccountTypeResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetAccountByType(string entityType, string entityId, [FromBody]AccountTypeRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetAccountByType(entityType, entityId, request)));
            });
        }

        [HttpPost("{entitytype}/{entityId}/finicity/events/{eventName}")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> VerifyFinicityEvents(string entityType, string entityId, string eventName)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.VerifyFinicityEvents(entityType, entityId, eventName)));
            });
        }

        [HttpPost("/link-bank/{bankId}/{entityType}/{entityId}")]
        public async Task<IActionResult> AddBankLink(string bankId, string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.AddBankLink(bankId, entityType, entityId);
                return NoContentResult;
            });
        }

        [HttpGet("/link-bank/{bankId}")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Cashflow.IBankLink), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetBankLink(string bankId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.GetBankLink(bankId));
            });
        }

        [HttpGet("{entitytype}/{entityId}/accounts/all")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Cashflow.IAccountTypeResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetAllAccounts(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetAllAccounts(entityType, entityId)));
            });
        }
        [HttpGet("/routing-number/{routingNumber}/account-number/{accountNumber}")]
#if DOTNET2
        [ProducesResponseType(typeof(List<IBankAccount>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetAccountByRoutingNumberAndAccountNumber(string routingNumber, string accountNumber)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetAccountByRoutingNumberAndAccountNumber(routingNumber, accountNumber)));
            });
        }

        [HttpGet("{entitytype}/{entityId}/transaction/{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Cashflow.ITransactionResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetAccountTransaction(string entityType, string entityId, string accountId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetAccountTransaction(entityType, entityId, accountId)));
            });
        }

#region Add Accounts - Bulk
        [HttpPost("{entitytype}/{entityId}/add/account/bulk")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Cashflow.IAddAccountBulkResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddBankAccountBulk(string entityType, string entityId, [FromBody]BulkAccountRequest bankAccount)
        {
            return Ok(await Task.Run(() => Service.AddBankAccountBulk(entityType, entityId, bankAccount)));
        }
#endregion

        [HttpPost("{entitytype}/{entityId}/add/transaction")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddBankTransaction(string entityType, string entityId, [FromBody]BankTransactionRequest bankTransaction)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.AddBankTransaction(entityType, entityId, bankTransaction);
                return Ok();
            });
        }

        [HttpPost("{entitytype}/{entityId}/add/transaction/bulk")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddBankTransactionBulk(string entityType, string entityId, [FromBody]BulkTransactionRequest bankTransaction)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.AddBankTransaction(entityType, entityId, bankTransaction);
                return Ok();
            });
        }

        [HttpPost("{entitytype}/{entityId}/calculate/runningbalance")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> CalculateRunningBalance(string entityType, string entityId, [FromBody]object data)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.CalculateRunningBalance(entityType, entityId, data);
                return Ok();
            });
        }

        [HttpGet("{entitytype}/{entityId}/calculate/runningbalance/all")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> CalculateRunningBalanceAll(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.CalculateRunningBalanceAll(entityType, entityId);
                return Ok();
            });
        }

    }
}