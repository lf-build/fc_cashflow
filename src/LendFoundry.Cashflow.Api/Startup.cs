﻿using LendFoundry.Application.Document.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.EventHub.Client;
using LendFoundry.EventHub;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.Cashflow.Persistence;
using LendFoundry.DocumentManager.Client;
using LendFoundry.ProductRule.Client;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using System.Runtime;
using System;


namespace LendFoundry.Cashflow.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env) { }

        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "BusinessApplicant"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Business.Applicant.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerDocumentation();
#endif

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            services.AddConfigurationService<CashflowConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddDataAttributes(Settings.DataAttribute.Host, Settings.DataAttribute.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddDocumentGenerator(Settings.DocumentGenerator.Host, Settings.DocumentGenerator.Port);
            services.AddApplicantDocumentService(Settings.ApplicationDocument.Host, Settings.ApplicationDocument.Port);
            services.AddDocumentManager(Settings.DocumentManager.Host, Settings.DocumentManager.Port);
            services.AddProductRuleService(Settings.ProductRule.Host, Settings.ProductRule.Port);
            //configurations
            services.AddTransient<ICashflowConfiguration>(p =>
            {
                var configuration = p.GetService<IConfigurationService<CashflowConfiguration>>().Get();
                return configuration;
            });
            // eventhub factory
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            // interface implements
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));

            // cashflow service

            services.AddTransient<ICashflowService, CashflowService>();
            services.AddTransient<ICashflowServiceFactory, CashflowServiceFactory>();

            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IAccountRepositoryFactory, AccountRepositoryFactory>();

            services.AddTransient<ILinkRepository, LinkRepository>();
            services.AddTransient<ILinkRepositoryFactory, LinkRepositoryFactory>();

            services.AddTransient<ITransactionRepository, TransactionRepository>();
            services.AddTransient<ITransactionRepositoryFactory, TransactionRepositoryFactory>();
            services.AddTransient<ICashflowListener, CashflowListener>();
            services.AddTransient<IAccountHistoryRepository, AccountHistoryRepository>();
            services.AddTransient<IAccountHistoryRepositoryFactory, AccountHistoryRepositoryFactory>();
            services.AddTransient<IPdfMergerService, PdfMergerService>();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Cashflow Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseCashflowListener();
        }
    }
}