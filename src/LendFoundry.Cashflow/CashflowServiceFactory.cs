﻿using LendFoundry.Application.Document.Client;
using LendFoundry.Cashflow.Persistence;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocumentManager.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.ProductRule;
using LendFoundry.ProductRule.Client;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.Cashflow
{
    public class CashflowServiceFactory : ICashflowServiceFactory
    {
        public CashflowServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ICashflowService Create(ITokenReader reader, ITokenHandler handler)
        {
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var logger = loggerFactory.Create(NullLogContext.Instance);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var cashflowConfigurationService = configurationServiceFactory.Create<CashflowConfiguration>(Settings.ServiceName, reader);
            var cashflowConfiguration = cashflowConfigurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory>();
            var dataAttributesService = dataAttributesClientFactory.Create(reader);

            var accountRepositoryFactory = Provider.GetService<IAccountRepositoryFactory>();
            var accountRepository = accountRepositoryFactory.Create(reader);

            var transactionRepositoryFactory = Provider.GetService<ITransactionRepositoryFactory>();
            var transactionRepository = transactionRepositoryFactory.Create(reader);

            var decisionEngineClientFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngineService = decisionEngineClientFactory.Create(reader);

            var applicationDocumentServiceClientFactory = Provider.GetService<IApplicationDocumentServiceClientFactory>();
            var applicationDocumentService = applicationDocumentServiceClientFactory.Create(reader);

            var documentGeneratorServiceFactory = Provider.GetService<IDocumentGeneratorServiceFactory>();
            var documentGeneratorService = documentGeneratorServiceFactory.Create(reader);


            var linkRepositoryFactory = Provider.GetService<ILinkRepositoryFactory>();
            var linkRepository = linkRepositoryFactory.Create(reader);

            var accountHistoryRepositoryFactory = Provider.GetService<IAccountHistoryRepositoryFactory>();
            var accountHistoryRepository = accountHistoryRepositoryFactory.Create(reader);

            var documentManagerRepositoryFactory = Provider.GetService<IDocumentManagerServiceFactory>();
            var documentManagerRepository = documentManagerRepositoryFactory.Create(reader);

            //    return new CashflowService(logger, eventHub, tenantTime, dataAttributesService,
            //        cashflowConfiguration, handler, reader, accountRepository, transactionRepository,
            //        decisionEngineService, applicationDocumentService, documentGeneratorService, linkRepository,
            //        accountHistoryRepository, documentManagerRepository, productRuleEngine);
            //}

            var productRuleFactory = Provider.GetService<IProductRuleServiceClientFactory>();
            var productRuleEngine = productRuleFactory.Create(reader);

            //return new CashflowService(logger, eventHub, tenantTime, dataAttributesService,
            //    cashflowConfiguration, handler,reader, accountRepository, transactionRepository, 
            //    decisionEngineService, applicationDocumentService, documentGeneratorService, productRuleEngine);

            return new CashflowService(logger, eventHub, tenantTime, dataAttributesService,
                cashflowConfiguration, handler, reader, accountRepository, transactionRepository,
                decisionEngineService, applicationDocumentService, documentGeneratorService, linkRepository,
                accountHistoryRepository, documentManagerRepository, productRuleEngine);
        }

    }
}