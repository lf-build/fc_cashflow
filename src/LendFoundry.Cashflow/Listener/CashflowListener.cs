﻿using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Reflection;

namespace LendFoundry.Cashflow
{
    public class CashflowListener : ICashflowListener
    {
#region Constructor
        public CashflowListener
        (
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ITenantTimeFactory tenantTimeFactory,
            ICashflowServiceFactory cashflowServicFactory,
            IConfigurationServiceFactory configurationFactory)
        {

            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(cashflowServicFactory), cashflowServicFactory);
            EnsureParameter(nameof(tenantTimeFactory), tenantTimeFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);

            EventHubFactory = eventHubFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            CashflowServiceFactory = cashflowServicFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;
        }
#endregion

#region Private Properties       
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ICashflowServiceFactory CashflowServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
#endregion

#region Public Methods

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");
                tenants.ForEach(tenant =>
                {
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName, null, "system", null);
                    var reader = new StaticTokenReader(token.Value);
                    var eventHub = EventHubFactory.Create(reader);
                    var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                    var configuration = ConfigurationFactory.Create<CashflowConfiguration>(Settings.ServiceName, reader).Get();

                    var cashflowService = CashflowServiceFactory.Create(reader, null);

                    if (configuration?.events != null)
                    {
                        configuration
                       .events
                       .ToList().ForEach(events =>
                       {
                           eventHub.On(events.Name, ProcessEvent(events, logger, cashflowService, eventHub));
                           logger.Info($"It was made subscription to EventHub with the Event: #{events.Name}");
                       });

                        logger.Info("-------------------------------------------");
                    }
                    else
                    {
                        logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                    }
                    eventHub.StartAsync();
                });

                logger.Info("Cashflow listener started");
            }
            catch (Exception ex)
            {
                logger.Error("Error while listening event hub to process Cashflow listener", ex);
                logger.Info("\n Cashflow listener  is working yet and waiting new event\n");
                Start();
            }
        }

#endregion

#region Private Methods

#region Process Event
        private Action<LendFoundry.EventHub.EventInfo> ProcessEvent(
           EventConfiguration eventConfiguration,
           ILogger logger,
           ICashflowService cashflowClientService,
           IEventHubClient eventHub
           )
        {
            return async @event =>
            {
                object objRequest = null;
                object objResponse = null;
                try
                {
                    try
                    {
                        var responseData = eventConfiguration.Response.FormatWith(@event);
                        objResponse = JsonConvert.DeserializeObject<dynamic>(responseData);
                    }
                    catch
                    {
                        // ignored
                    }
                    try
                    {
                        var requestData = eventConfiguration.Request.FormatWith(@event);
                        objRequest = JsonConvert.DeserializeObject<dynamic>(requestData);
                    }
                    catch
                    {
                        // ignored
                    }

                    object eventData = new { Request = objRequest, Response = objResponse };

                    var eventRequest = new EventRequest();

                    var entityId = eventConfiguration.EntityId.FormatWith(@event);
                    var entityType = eventConfiguration.EntityType.FormatWith(@event);
                    var name = eventConfiguration.Name.FormatWith(@event);

                    eventRequest.DERuleName = eventConfiguration.DERuleName.FormatWith(@event);
                    eventRequest.InitiateCashflow = eventConfiguration.InitiateCashflow;
                    eventRequest.data = eventData;


                    var methodName = eventConfiguration.MethodToExecute;
                    MethodInfo method = typeof(ICashflowService).GetMethod(methodName);
                    object[] param = { entityType, entityId, eventRequest.InitiateCashflow? objResponse : eventRequest };
                    
                    if (method != null)
                    {
                        var result = await (dynamic)method.Invoke(cashflowClientService, param);
                        if (!string.IsNullOrEmpty(eventConfiguration.CompletionEventName))
                        {
                            await eventHub.Publish(eventConfiguration.CompletionEventName, new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = eventData,
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                        }
                    }
                    else
                    {
                        logger.Warn("Method not found" + methodName);
                    }

                    logger.Info($"Entity Id: #{entityId}");
                    logger.Info($"Response Data : #{eventData}");
                    logger.Info($"Cashflow event {name} completed for {entityId} ");

                }
                catch (Exception ex)
                {
                    logger.Error($"Unhandled exception while listening event {@event.Name}", ex);
                }
            };
        }
#endregion

#region Ensure Parameter
        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));
        }
#endregion

#region Execute Request
/*
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
*/
#endregion

#endregion

    }
}
