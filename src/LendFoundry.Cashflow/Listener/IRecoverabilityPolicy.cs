﻿using System;

namespace LendFoundry.Cashflow
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}
