﻿
using LendFoundry.Security.Tokens;

namespace LendFoundry.Cashflow
{
    public interface ICashflowServiceFactory
    {
        ICashflowService Create(ITokenReader reader, ITokenHandler handler);
    }
}
