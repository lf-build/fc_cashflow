﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Cashflow
{
    public static class CashflowListenerExtensions
    {
        public static void UseCashflowListener(this IApplicationBuilder application)
        {
            application.ApplicationServices.GetRequiredService<ICashflowListener>().Start();
        }
    }
}
