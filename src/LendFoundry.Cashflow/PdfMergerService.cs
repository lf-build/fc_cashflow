﻿using System.Collections.Generic;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace LendFoundry.Cashflow
{
    public class PdfMergerService : IPdfMergerService
    {
        public byte[] MergePdf(IEnumerable<Stream> streams)
        {
            using (var outPdf = new PdfDocument())
            {
                foreach (var stream in streams)
                {
                    using (var from = PdfReader.Open(stream, PdfDocumentOpenMode.Import))
                    {
                        for (var i = 0; i < from.PageCount; i++)
                        {
                            outPdf.AddPage(from.Pages[i]);
                        }
                    }
                }

                using (var outputStream = new MemoryStream())
                {
                    outPdf.Save(outputStream, false);

                    outputStream.Position = 0;

                    return outputStream.ToArray();
                }
            }
        }

    }
}
