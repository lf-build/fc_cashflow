﻿using LendFoundry.Application.Document;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DataAttributes;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.DocumentGenerator;
using LendFoundry.TemplateManager;
using System.Text;
using LendFoundry.Cashflow.Persistence;
using System.IO;
using docManager = LendFoundry.DocumentManager;
using LendFoundry.ProductRule;


namespace LendFoundry.Cashflow
{
    public class CashflowService : ICashflowService
    {
        #region Constructor

        public CashflowService
        (
            ILogger logger,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime,
            IDataAttributesEngine dataAttributesService,
            ICashflowConfiguration cashflowConfigurationService,
            ITokenHandler tokenParser,
            ITokenReader tokenReader,
            IAccountRepository accountRepository,
            ITransactionRepository transactionRepository,
            IDecisionEngineService decisionEngineService,
            IApplicationDocumentService applicationDocumentService,
            IDocumentGeneratorService documentGeneratorService,

            ILinkRepository linkRepository,
            IAccountHistoryRepository accountHistoryRepository,
            docManager.IDocumentManagerService documentManagerService,
            IProductRuleService productRuleService

        )
        {
            Logger = logger;
            EventHub = eventHubClient;
            TenantTime = tenantTime;
            DataAttributesService = dataAttributesService;
            CashflowConfigurationService = cashflowConfigurationService;
            TokenParser = tokenParser;
            TokenReader = tokenReader;
            AccountRepository = accountRepository;
            DecisionEngineService = decisionEngineService;
            TransactionRepository = transactionRepository;
            ApplicationDocumentService = applicationDocumentService;
            DocumentGeneratorService = documentGeneratorService;
            LinkRepository = linkRepository;
            AccountHistoryRepository = accountHistoryRepository;
            DocumentManagerService = documentManagerService;
            PdfMergerService = new PdfMergerService();
            ProductRuleService = productRuleService;
        }
        #endregion

        #region Private Variables


        private IApplicationDocumentService ApplicationDocumentService { get; set; }
        private IDocumentGeneratorService DocumentGeneratorService { get; set; }
        private IAccountRepository AccountRepository { get; }
        private ILinkRepository LinkRepository { get; }
        private ITransactionRepository TransactionRepository { get; }
        private ILogger Logger { get; }
        private ITokenHandler TokenParser { get; }
        private ITokenReader TokenReader { get; }
        private IEventHubClient EventHub { get; }
        private ITenantTime TenantTime { get; }
        private ICashflowConfiguration CashflowConfigurationService { get; }
        private IDataAttributesEngine DataAttributesService { get; }
        private IDecisionEngineService DecisionEngineService { get; }

        private IAccountHistoryRepository AccountHistoryRepository { get; }
        private docManager.IDocumentManagerService DocumentManagerService { get; }
        private IPdfMergerService PdfMergerService { get; set; }
       
        private IProductRuleService ProductRuleService { get; set; }
        #endregion

        public async Task<IAddAccountResponse> AddBankAccount(string entityType, string entityId, IBankAccountRequest BankAccount)
        {
            try
            {
                Logger.Info("Started Execution for AddBankAccount at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                var request = new BankAccount(BankAccount);
                Logger.Info("Completed Execution for AddBankAccount at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                return await AddBankAccount(entityType, entityId, request, true);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddBankAccount Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message);
                throw;
            }
        }


        public async Task<IAddAccountBulkResponse> AddBankAccountBulk(string entityType, string entityId, IBulkAccountRequest BankAccount)
        {
            IAddAccountBulkResponse result = new AddAccountBulkResponse();
            result.bankAccounts = new List<IAccountBulkResponse>();
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (BankAccount == null)
                throw new ArgumentNullException(nameof(BankAccount));

            if (BankAccount.bankAccount == null)
                throw new ArgumentNullException(nameof(BankAccount.bankAccount), "Bank account list is not available");

            try
            {
                Logger.Info("Started Execution for AddBankAccount bulk at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                var accounts = await AccountRepository.AddBankAccount(entityType, entityId, BankAccount.bankAccount);
                foreach (var item in accounts)
                {
                    result.bankAccounts.Add(new AccountBulkResponse(item.Id, item.ProviderAccountId));
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error While Processing AddBankAccount bulk at:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception :" + ex.Message + ex.StackTrace);
                throw;
            }
            Logger.Info("Ended Execution for AddBankAccount bulk at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }

        private async Task<IAddAccountResponse> AddBankAccount(string entityType, string entityId, IBankAccount request, bool raiseCashflowEvent = false)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (string.IsNullOrWhiteSpace(request.Source))
            {
                request.Source = CashflowConfigurationService.Source;
            }
            if (request.BalanceAsOfDate == null)
            {
                request.BalanceAsOfDate = new TimeBucket(TenantTime.Now.UtcDateTime);
            }

            request.EntityId = entityId;
            request.EntityType = entityType;
            request.UpdatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
            request.UpdatedBy = EnsureCurrentUser();
            request.Refreshstatus = true;
            request.LastRefreshDate = new TimeBucket(TenantTime.Now.UtcDateTime);
            request.ProviderErrorCode = "";
            request.ProviderErrorDescription = "";

            var objApplicationDataAttribute = await DataAttributesService.GetAttribute(entityType, entityId, "application");
            if (objApplicationDataAttribute != null)
            {
                var lstApplicationResult = (JArray)objApplicationDataAttribute;
                if (lstApplicationResult != null && lstApplicationResult.Count > 0)
                {
                    request.NameOnAccount = lstApplicationResult[0]["businessApplicantName"] + "";
                }
            }
            if (string.IsNullOrEmpty(request.Id))
            {
                request.CreatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
                request.CreatedBy = EnsureCurrentUser();
            }
            else
            {
                await AccountHistoryRepository.UpdateAccountHistory(request);
            }

            var accountId = await AccountRepository.AddBankAccount(request);

            // Add bank link for respective entity type and entity id.
            await AddBankLink(accountId, entityType, entityId);

            if (raiseCashflowEvent)
            {
                //edit account
                var accounts = await AccountRepository.GetAccountDetails(entityType, entityId, request.Id);

                await CalculateCashflow(entityType, entityId, CashflowConfigurationService.RuleName["plaid"], accounts, ProductRuleService, DataAttributesService);
            }
            var response = new AddAccountResponse();
            response.AccountId = accountId;

            if (!string.IsNullOrEmpty(request.Id))
            {
                await EventHub.Publish("CashflowUpdated", new
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = request.AccountNumber,
                    Request = entityId,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }

            return response;
        }

        public async Task<string> AddBankTransaction(string entityType, string entityId, IBulkTransactionRequest BankTransaction)
        {
            string result = string.Empty;
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (BankTransaction == null)
                throw new ArgumentNullException(nameof(BankTransaction));

            if (BankTransaction.bankTransaction == null)
                throw new ArgumentNullException(nameof(BankTransaction.bankTransaction), "Bank transaction list is not available");

            try
            {
                Logger.Info("Started Execution for AddBankTransaction bulk at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

                await TransactionRepository.AddTransaction(BankTransaction.bankTransaction);
                result = "success";
            }
            catch (Exception ex)
            {
                Logger.Error("Error While Processing AddBankTransaction bulk at:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception :" + ex.Message + ex.StackTrace);

                throw;
            }
            Logger.Info("Ended Execution for AddBankTransaction bulk at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }
        public async Task<string> AddBankTransaction(string entityType, string entityId, IBankTransactionRequest bankTransaction)
        {
            var request = new Transaction(bankTransaction);
            return await AddBankTransaction(entityType, entityId, request);
        }

        private async Task<string> AddBankTransaction(string entityType, string entityId, ITransaction request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (request == null)
                throw new ArgumentNullException(nameof(request));


            request.EntityId = entityId;
            request.EntityType = entityType;
            if (string.IsNullOrEmpty(request.Id))
            {
                request.CreatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
            }
            else
            {
                request.UpdatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
            }
            return await TransactionRepository.AddTransaction(request);
        }

        private string EnsureCurrentUser()
        {
            if (TokenParser != null)
            {
                var token = TokenParser.Parse(TokenReader.Read());
                var username = token?.Subject;
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new ArgumentException("User is not authorized");
                return username;
            }
            return null;
        }

        public async Task AddCashFlowData(string entityType, string entityId, ICashflowRequest cashflowRequest)
        {
            try
            {
                Logger.Info("Started Execution for AddCashFlowData at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (cashflowRequest == null)
                    throw new ArgumentNullException(nameof(cashflowRequest));
                var data = new CashflowRequest(cashflowRequest);
                //await DataAttributesService.SetAttribute(entityType, entityId, "plaidCashflow", data, data.AccountID);
                await DataAttributesService.SetAttribute(entityType, entityId, CashflowConfigurationService.DataAttributeStoreName, data, data.AccountID);

                await EventHub.Publish("CashflowUpdated", new
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = data.AccountNumber,
                    Request = entityId,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                Logger.Info("Completed Execution for AddCashFlowData at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddCashFlowData Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message);

                throw;
            }
        }

        public async Task<string> GetBankName(string routingNumber)
        {
            if (string.IsNullOrWhiteSpace(routingNumber))
                throw new ArgumentNullException(nameof(routingNumber));
            try
            {
                Logger.Info("Started Execution for GetBankName at:" + TenantTime.Now);
                var restRequest = new RestRequest(Method.GET);
                var uri = new Uri(CashflowConfigurationService.BanklookupUrl);
                restRequest.AddParameter("RoutingNumber", routingNumber, ParameterType.QueryString);
                var client = new RestClient(uri);
                var response = await client.ExecuteTaskAsync(restRequest);

                if (response == null)
                    throw new NotFoundException("Bank name not available for provided RoutingNumber");

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var xmalResponse = System.Net.WebUtility.HtmlDecode(response.Content);

                    var result = XmlDeserialization.Deserialize<BankDetails>(xmalResponse);

                    if (result == null)
                        throw new NullReferenceException(nameof(result));

                    if (result.NewDataSet == null)
                        throw new NullReferenceException(nameof(result.NewDataSet));

                    if (result.NewDataSet.Table == null)
                        throw new NotFoundException("Bank name not available for provided RoutingNumber");

                    Logger.Info("Completed Execution for GetBankName at:" + TenantTime.Now);

                    return result.NewDataSet.Table.BankName.Trim();
                }
                else
                {
                    return "fake_institution";
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddCashFlowData Date & Time:" + TenantTime.Now + "Exception" + exception.Message);

                throw;
            }
        }

        public async Task<bool> AddAccountPreference(string entityType, string entityId, IAccountPreferenceRequest accountPreference)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));


            if (accountPreference == null)
                throw new ArgumentNullException(nameof(accountPreference));
            try
            {
                Logger.Info("Started Execution for AddAccountPreference at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                var data = new AccountPreference(accountPreference);
                if (data.IsCashflowAccount.HasValue && data.IsCashflowAccount.Value)
                {
                    await AccountRepository.ResetAccountPreference(entityType, entityId, AccountType.Cashflow);
                    await EventHub.Publish("CashflowAccountSelected", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = new { dataAttributeName = accountPreference.AccountID },
                        Request = entityId,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                }
                else if (data.IsFundingAccount.HasValue && data.IsFundingAccount.Value)
                {
                    var account = await AccountRepository.GetAccountDetails(entityType, entityId, data.AccountID);
                    await DataAttributesService.SetAttribute(entityType, entityId, "fundingAccount", account);
                    await AccountRepository.ResetAccountPreference(entityType, entityId, AccountType.Funding);
                }
                Logger.Info("Completed Execution for AddAccountPreference at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                return await AccountRepository.UpdateAccountPreference(data);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddAccountPreference Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message);
                throw;
            }
        }

        public async Task<IBankAccount> GetBankAccountDetails(string entityType, string entityId, string accountId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            return await AccountRepository.GetAccountDetails(entityType, entityId, accountId);
        }

        public async Task<IBankAccount> GetBankAccountDetailsById(string accountId)
        {
            return await AccountRepository.GetAccountDetailsById(accountId);
        }

        #region Private Methods

        /*
                private string RemoveSpace(string value)
                {
                    return value.Trim().Replace(" ", "");
                }
        */

        #region Execute Request
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
        #endregion

        #endregion Private Methods

        public async Task<bool> ExtractAccountsAndTransactionsHandler(string entityType, string entityId, object request)
        {
            var result = false;
            try
            {
                Logger.Info("Started Execution for ExtractAccountsAndTransactionsHandler at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (request == null)
                    throw new ArgumentNullException(nameof(request));

                var objAccount = JsonConvert.DeserializeObject<Dictionary<string, string>>(request.ToString());

                var accountId = objAccount["AccountId"];
                var IsSelected = Convert.ToBoolean(objAccount["IsSelected"]);
                var lstAccounts = new List<string>();
                lstAccounts.Add(accountId);

                var accounts = await AccountRepository.GetAccountDetails(entityType, entityId, accountId);

                await CalculateCashflow(entityType, entityId, CashflowConfigurationService.RuleName["plaid"], accounts, ProductRuleService, DataAttributesService);

                #region Account Preference

                if (IsSelected == true)
                {
                    await DataAttributesService.SetAttribute(entityType, entityId, "fundingAccount", accounts);
                    await EventHub.Publish("CashflowAccountSelected", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = new { dataAttributeName = accounts.Id },
                        Request = entityId,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                }
                #endregion


                #region PDF Creation

                if (CashflowConfigurationService.CashflowReportConfig.Consolidated.IsActive)
                {
                    await GenerateAndSaveCashflowReport(entityType, entityId, lstAccounts,
                          DataAttributesService,
                          CashflowConfigurationService, DocumentGeneratorService, ApplicationDocumentService, Logger);
                }
                #endregion

                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error($"ExtractAccountsAndTransactionsHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
            Logger.Info("Completed Execution for ExtractAccountsAndTransactionsHandler at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }

        private async Task CalculateCashflow(string entityType, string entityId, string RuleName, IBankAccount accounts,
            IProductRuleService productRuleService, IDataAttributesEngine dataAttributesService)
        {
            Logger.Info("Started Execution for CalculatePlaidCashflow at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            try
            {
                var productId = string.Empty;
                var objApplicationDataAttribute = await dataAttributesService.GetAttribute(entityType, entityId, "product");
                if (objApplicationDataAttribute != null)
                {
                    var lstApplicationResult = (JArray)objApplicationDataAttribute;
                    if (lstApplicationResult != null && lstApplicationResult.Count > 0)
                    {
                        var json = (JObject)JsonConvert.DeserializeObject(lstApplicationResult[0] + "");

                        Dictionary<string, object> d = new Dictionary<string, object>(json.ToObject<IDictionary<string, object>>(), StringComparer.CurrentCultureIgnoreCase);

                        productId = d["ProductId"] + "";
                    }
                }

                var objRuleResult = await productRuleService.RunRule(entityType, entityId, productId, RuleName, new { Accounts = accounts });

                if (objRuleResult != null && objRuleResult.IntermediateData != null)
                {
                    var intermediateResult = JsonConvert.DeserializeObject<Dictionary<string, object>>(objRuleResult.IntermediateData.ToString());
                    if (intermediateResult != null && intermediateResult.Any())
                    {
                        foreach (var attribute in intermediateResult)
                        {
                            await dataAttributesService.SetAttribute(entityType, entityId, CashflowConfigurationService.DataAttributeStoreName, attribute.Value, attribute.Key);
                        }
                    }
                }

                await CalculateRunningBalance(entityType, entityId, accounts, TransactionRepository);

                Logger.Info("Ended Execution for CalculatePlaidCashflow at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            }
            catch (Exception ex)
            {
                Logger.Error($"CalculatePlaidCashflow Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
        }

        private async Task SaveAccountAndTransaction(string entityType, string entityId, dynamic ruleResult,
            IDataAttributesEngine dataAttributesService,
            ICashflowConfiguration cashflowConfigurationService,
            IDocumentGeneratorService documentGeneratorService,
            IApplicationDocumentService applicationDocumentService,
            ILogger Logger,
            bool initiateCashflow,

            IProductRuleService productRuleService)
        {
            List<string> lstAccounts = new List<string>();
            AccountsAndTransactions objInput = new AccountsAndTransactions();
            objInput = ExecuteRequest<AccountsAndTransactions>(ruleResult + "");
            bool IsTransactionsAvailable = false;
            var allTransactions = new List<ITransaction>();
            if (objInput.Transactions != null && objInput.Transactions.Count > 0)
            {
                allTransactions = new List<ITransaction>(objInput.Transactions.Cast<ITransaction>());
                IsTransactionsAvailable = true;
            }

            if (objInput.Accounts != null && objInput.Accounts.Count > 0)
            {
                var selectedCashflowAccountId = string.Empty;

                foreach (var account in objInput.Accounts)
                {
                    var objAccountTransaction = new List<ITransaction>();
                    var objResponse = await AddBankAccount(entityType, entityId, account, false);
                    var accountId = string.Empty;
                    if (objResponse != null)
                    {
                        accountId = objResponse.AccountId;
                    }

                    lstAccounts.Add(accountId);
                    if (account.IsCashflowAccount)
                    {
                        selectedCashflowAccountId = accountId;
                        await dataAttributesService.SetAttribute(entityType, entityId, "fundingAccount", account);
                    }
                    if (IsTransactionsAvailable)
                    {
                        objAccountTransaction = allTransactions.Where(t => t.ProviderAccountId == account.ProviderAccountId).ToList();
                        if (objAccountTransaction != null && objAccountTransaction.Count > 0)

                        {
                            objAccountTransaction.ForEach(t => t.AccountId = accountId);
                        }
                        foreach (var transaction in objAccountTransaction)
                        {
                            var TransactionDate_OffSet = Convert.ToDateTime(transaction.TransactionDate);
                            transaction.TransactionOn = new TimeBucket(TransactionDate_OffSet);
                            await AddBankTransaction(entityType, entityId, transaction);
                        }
                    }

                    if (initiateCashflow)
                        await CalculateCashflow(entityType, entityId, CashflowConfigurationService.RuleName["plaid"], account,
                            productRuleService, dataAttributesService);

                }

                if (!string.IsNullOrEmpty(selectedCashflowAccountId))
                {
                    await EventHub.Publish("CashflowAccountSelected", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = new { dataAttributeName = selectedCashflowAccountId },
                        Request = entityId,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                }

            }
            if (cashflowConfigurationService.CashflowReportConfig.Consolidated.IsActive && initiateCashflow)
            {
                await GenerateAndSaveCashflowReport(entityType, entityId, lstAccounts,
                       dataAttributesService,
                       cashflowConfigurationService, documentGeneratorService, applicationDocumentService, Logger);
            }
        }

        private string FormatMoney(double amount)
        {
            string result = string.Empty;
            var usCulture = CultureInfo.CreateSpecificCulture("en-US");
            var clonedNumbers = (NumberFormatInfo)usCulture.NumberFormat.Clone();
            clonedNumbers.CurrencyNegativePattern = 0;
            result = amount.ToString("C", clonedNumbers);
            return result;
        }

        private string FormatPercentage(double amount)
        {
            /*string result = string.Empty;
            var usCulture = CultureInfo.CreateSpecificCulture("en-US");
            var clonedNumbers = (NumberFormatInfo)usCulture.NumberFormat.Clone();
            clonedNumbers.CurrencyNegativePattern = 0;
            result = amount.ToString("P", clonedNumbers);*/

            var result = string.Format("{0:0.00}", amount) + "%";
            return result;
        }

        #region Cashflow PDF Report 
        private async Task GenerateAndSaveCashflowReport(string entityType, string entityId, List<string> data,
            IDataAttributesEngine dataAttributesService,
            ICashflowConfiguration cashflowConfigurationService,
            IDocumentGeneratorService documentGeneratorService,
            IApplicationDocumentService applicationDocumentService,
            ILogger Logger)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (data == null)
                    throw new ArgumentNullException(nameof(data));

                var objApplicationDataAttribute = await dataAttributesService.GetAttribute(entityType, entityId, "application");
                if (objApplicationDataAttribute != null)
                {
                    var AccountHolderName = string.Empty;
                    var Email = string.Empty;
                    var lstApplicationResult = (JArray)objApplicationDataAttribute;
                    if (lstApplicationResult != null && lstApplicationResult.Count > 0)
                    {
                        AccountHolderName = lstApplicationResult[0]["businessApplicantName"] + "";
                        Email = lstApplicationResult[0]["applicantWorkEmail"] + "";
                    }
                    foreach (var key in data)
                    {
                        var name = key;
                        object objDataAttribute = null;
                        FaultRetry.RunWithAlwaysRetry(() =>
                        {
                            objDataAttribute = dataAttributesService.GetAttribute(entityType, entityId, cashflowConfigurationService.DataAttributeStoreName, name).Result;
                            if (objDataAttribute == null)
                                throw new InvalidOperationException($"{name} not found for EntityId : {entityId}");
                        });
                        if (objDataAttribute == null)
                            throw new InvalidOperationException($"{name} not found for EntityId : {entityId}");
                        if (objDataAttribute != null)
                        {
                            var lstCashFlowResult = (JArray)objDataAttribute;
                            var item = new CashflowTemplate();
                            if (lstCashFlowResult != null && lstCashFlowResult.Count > 0)
                            {
                                item = lstCashFlowResult[0].ToObject<CashflowTemplate>();
                            }
                            ICashflowDocument objCashflowReport = new CashflowDocument();
                            ICashflowDocument objMonthlyReport = new CashflowDocument();
                            //ICashflowGridDocument objMonthlyGridReport = new CashflowGridDocument();

                            if (item != null)
                            {
                                objCashflowReport.Logo = cashflowConfigurationService.CashflowReportConfig.Logo;

                                #region Account Section

                                objCashflowReport.CashflowAccountSection = new CashflowAccountSection();
                                objCashflowReport.CashflowAccountSection.AccountNumber = item.AccountNumber;
                                objCashflowReport.CashflowAccountSection.AccountHolderName = AccountHolderName;
                                objCashflowReport.CashflowAccountSection.LoanNumber = entityId;
                                objCashflowReport.CashflowAccountSection.InstitutionName = item.InstitutionName;
                                objCashflowReport.CashflowAccountSection.AccountType = item.AccountType;
                                objCashflowReport.CashflowAccountSection.Email = Email;
                                objCashflowReport.CashflowAccountSection.MonthPeriod = item.MonthPeriod;
                                #endregion

                                if (item.CashFlow != null)
                                {
                                    #region Category Summary Section

                                    if (item.CashFlow.CategorySummary != null && item.CashFlow.CategorySummary.Count > 0)
                                    {
                                        objCashflowReport.CashflowCategorySummary = new List<CashflowCategorySummarySection>();
                                        foreach (var category in item.CashFlow.CategorySummary)
                                        {
                                            if (!string.IsNullOrEmpty(category.CategoryName))
                                            {
                                                var objSummary = new CashflowCategorySummarySection();
                                                objSummary.CategoryId = category.CategoryId;
                                                objSummary.CategoryName = category.CategoryName;
                                                objSummary.CustomCategoryId = category.CustomCategoryId;
                                                objSummary.LastMonthTransactionCount = category.LastMonthTransactionCount + "";
                                                objSummary.LastMonthTransactionTotal = FormatMoney(category.LastMonthTransactionTotal);
                                                objSummary.TransactionCount = category.TransactionCount + "";
                                                objSummary.TransactionTotal = FormatMoney(category.TransactionTotal);
                                                objCashflowReport.CashflowCategorySummary.Add(objSummary);
                                            }
                                        }
                                        objCashflowReport.CashflowCategorySummary = objCashflowReport.CashflowCategorySummary.OrderBy(x => x.CustomCategoryId).ToList();
                                    }
                                    #endregion

                                    #region Transaction Summary Section

                                    if (item.CashFlow.TransactionSummary != null)
                                    {
                                        objCashflowReport.TransactionSummary = new TransactionSummarySection();
                                        objCashflowReport.TransactionSummary.AsOfDate = DateTime.UtcNow.ToString("MM/dd/yyyy");
                                        objCashflowReport.TransactionSummary.AverageDailyBalance = FormatMoney(item.CashFlow.TransactionSummary.AverageDailyBalance);                                      
                                        objCashflowReport.TransactionSummary.AvailableBalance = FormatMoney(item.CashFlow.TransactionSummary.AvailableBalance);
                                        objCashflowReport.TransactionSummary.CurrentBalance = FormatMoney(item.CashFlow.TransactionSummary.CurrentBalance);
                                        objCashflowReport.TransactionSummary.TotalCredits = FormatMoney(item.CashFlow.TransactionSummary.TotalCredits);
                                        objCashflowReport.TransactionSummary.TotalDebits = FormatMoney(item.CashFlow.TransactionSummary.TotalDebits);
                                        objCashflowReport.TransactionSummary.AverageBalanceLastMonth = FormatMoney(item.CashFlow.TransactionSummary.AverageBalanceLastMonth);
                                        objCashflowReport.TransactionSummary.AverageDeposit = FormatMoney(item.CashFlow.TransactionSummary.AverageDeposit);
                                        objCashflowReport.TransactionSummary.AverageWithdrawal = FormatMoney(item.CashFlow.TransactionSummary.AverageWithdrawal);
                                        objCashflowReport.TransactionSummary.ChangeInDepositVolume = item.CashFlow.TransactionSummary.ChangeInDepositVolume + "";
                                        objCashflowReport.TransactionSummary.StartDate = item.CashFlow.TransactionSummary.StartDate;
                                        objCashflowReport.TransactionSummary.EndDate = item.CashFlow.TransactionSummary.EndDate;
                                        objCashflowReport.TransactionSummary.NumberOfNegativeBalance = item.CashFlow.TransactionSummary.NumberOfNegativeBalance + "";
                                        objCashflowReport.TransactionSummary.CountOfMonthlyStatement = item.CashFlow.TransactionSummary.CountOfMonthlyStatement + "";
                                        objCashflowReport.TransactionSummary.TotalCreditsCount = item.CashFlow.TransactionSummary.TotalCreditsCount + "";
                                        objCashflowReport.TransactionSummary.TotalDebitsCount = item.CashFlow.TransactionSummary.TotalDebitsCount + "";
                                        objCashflowReport.TransactionSummary.AnnualCalculatedRevenue = FormatMoney(item.CashFlow.TransactionSummary.AnnualCalculatedRevenue);
                                        objCashflowReport.TransactionSummary.CVOfDailyBalance = FormatPercentage(item.CashFlow.TransactionSummary.CVOfDailyBalance);
                                        objCashflowReport.TransactionSummary.CVOfDailyDeposit = FormatPercentage(item.CashFlow.TransactionSummary.CVOfDailyDeposit);
                                        objCashflowReport.TransactionSummary.MedianDailyBalance = FormatMoney(item.CashFlow.TransactionSummary.MedianDailyBalance);
                                        objCashflowReport.TransactionSummary.MedianMonthlyIncome = FormatMoney(item.CashFlow.TransactionSummary.MedianMonthlyIncome);
                                        objCashflowReport.TransactionSummary.MaxDaysBelow100Count = item.CashFlow.TransactionSummary.MaxDaysBelow100Count + "";
                                        if (!string.IsNullOrEmpty(item.CashFlow.TransactionSummary.BrokerAGSText))
                                        {
                                            objCashflowReport.TransactionSummary.BrokerAGS = FormatMoney(item.CashFlow.TransactionSummary.BrokerAGS) + "[" + item.CashFlow.TransactionSummary.BrokerAGSText + "]";
                                        }
                                        else
                                        {
                                            objCashflowReport.TransactionSummary.BrokerAGS = FormatMoney(item.CashFlow.TransactionSummary.BrokerAGS);
                                        }
                                        objCashflowReport.TransactionSummary.CAPAGS = FormatMoney(item.CashFlow.TransactionSummary.CAPAGS);
                                    }

                                    #endregion

                                    #region Get Transactions

                                    var lstTransactionList = new List<ITransaction>();
                                    if (cashflowConfigurationService.CashflowReportConfig.Consolidated.Transactions == true ||
                                        (cashflowConfigurationService.CashflowReportConfig.MonthlySummary.IsActive == true &&
                                        cashflowConfigurationService.CashflowReportConfig.MonthlySummary.Transactions == true))
                                    {
                                        lstTransactionList = await TransactionRepository.GetAllBankTransaction(entityType, entityId, name);
                                    }
                                    #endregion

                                    List<string> lstMonthlyDocumentIds = new List<string>();

                                    if (cashflowConfigurationService.CashflowReportConfig.MonthlySummary.IsActive == true)
                                    {
                                        #region Monthly Summary Section
                                        if (item.CashFlow.MonthlyCashFlows != null && item.CashFlow.MonthlyCashFlows.Count > 0)
                                        {
                                            objMonthlyReport.CashflowAccountSection = objCashflowReport.CashflowAccountSection;
                                            objMonthlyReport.Logo = objCashflowReport.Logo;

                                            objMonthlyReport.MonthlySummary = new MonthlyCashFlowSummarySection();

                                            foreach (var monthSummary in item.CashFlow.MonthlyCashFlows)
                                            {
                                                #region Monthly Summary Section

                                                objMonthlyReport.MonthlySummary.FirstTransactionDate = monthSummary.FirstTransactionDate;
                                                objMonthlyReport.MonthlySummary.EndTransactionDate = monthSummary.EndTransactionDate;
                                                objMonthlyReport.MonthlySummary.LoanPaymentAmount = FormatMoney(monthSummary.LoanPaymentAmount);
                                                objMonthlyReport.MonthlySummary.MaxDepositAmount = FormatMoney(Convert.ToDouble(monthSummary.MaxDepositAmount));
                                                objMonthlyReport.MonthlySummary.MaxWithdrawalAmount = FormatMoney(Convert.ToDouble(monthSummary.MaxWithdrawalAmount));
                                                objMonthlyReport.MonthlySummary.MinDepositAmount = FormatMoney(Convert.ToDouble(monthSummary.MinDepositAmount));
                                                objMonthlyReport.MonthlySummary.MinWithdrawalAmount = FormatMoney(Convert.ToDouble(monthSummary.MinWithdrawalAmount));
                                                objMonthlyReport.MonthlySummary.NSFAmount = FormatMoney(monthSummary.NSFAmount);
                                                objMonthlyReport.MonthlySummary.Name = monthSummary.Name;
                                                objMonthlyReport.MonthlySummary.NumberOfLoanPayment = monthSummary.NumberOfLoanPayment + "";
                                                objMonthlyReport.MonthlySummary.NumberOfNSF = monthSummary.NumberOfNSF + "";
                                                objMonthlyReport.MonthlySummary.NumberOfNegativeBalance = monthSummary.NumberOfNegativeBalance + "";
                                                objMonthlyReport.MonthlySummary.NumberOfPayroll = monthSummary.NumberOfPayroll + "";
                                                objMonthlyReport.MonthlySummary.PayrollAmount = FormatMoney(monthSummary.PayrollAmount);
                                                objMonthlyReport.MonthlySummary.PeriodFrom = monthSummary.PeriodFrom;
                                                objMonthlyReport.MonthlySummary.PeriodTo = monthSummary.PeriodTo;
                                                objMonthlyReport.MonthlySummary.TotalDepositAmount = FormatMoney(monthSummary.TotalDepositAmount);
                                                objMonthlyReport.MonthlySummary.TotalWithdrawalAmount = FormatMoney(monthSummary.TotalWithdrawalAmount);
                                                objMonthlyReport.MonthlySummary.WithdrawalCount = monthSummary.WithdrawalCount + "";
                                                objMonthlyReport.MonthlySummary.Year = monthSummary.Year;
                                                objMonthlyReport.MonthlySummary.DateOfMonthlyCycle = monthSummary.DateOfMonthlyCycle;
                                                objMonthlyReport.MonthlySummary.NumberOfSpecialCategoryDeposit = monthSummary.NumberOfSpecialCategoryDeposit;
                                                objMonthlyReport.MonthlySummary.BeginingBalance = FormatMoney(Convert.ToDouble(monthSummary.BeginingBalance));
                                                objMonthlyReport.MonthlySummary.EndingBalance = FormatMoney(Convert.ToDouble(monthSummary.EndingBalance));
                                                objMonthlyReport.MonthlySummary.AverageDailyBalance = FormatMoney(Convert.ToDouble(monthSummary.AverageDailyBalance));
                                                objMonthlyReport.MonthlySummary.AverageDailyBalancePercent = FormatMoney(Convert.ToDouble(monthSummary.AverageDailyBalancePercent));
                                                objMonthlyReport.MonthlySummary.DepositCount = monthSummary.DepositCount + "";

                                                #endregion

                                                if (cashflowConfigurationService.CashflowReportConfig.MonthlySummary.Transactions)
                                                {
                                                    #region Transaction List
                                                    if (lstTransactionList != null && lstTransactionList.Count > 0)
                                                    {
                                                        objMonthlyReport.TransactionList = new List<CashflowTransactionListSection>();
                                                        var objCurrentMonthTransactions = lstTransactionList.Where(t => t.TransactionOn.Time.Year == monthSummary.Year && CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(t.TransactionOn.Time.Month) == monthSummary.Name).ToList();
                                                        if (objCurrentMonthTransactions != null && objCurrentMonthTransactions.Count > 0)
                                                        {
                                                            objCurrentMonthTransactions = objCurrentMonthTransactions.OrderByDescending(i => i.TransactionOn.Time).ToList();
                                                            foreach (var trans in objCurrentMonthTransactions)
                                                            {
                                                                var objTrans = new CashflowTransactionListSection();
                                                                objTrans.Amount = FormatMoney(trans.Amount);
                                                                objTrans.Description = trans.Description;
                                                                objTrans.Date = trans.TransactionOn.Time.ToString("MM/dd/yyyy");
                                                                objTrans.TransactionType = trans.Amount > 0 ? "Debit" : "Credit";
                                                                objTrans.RunningBalance = FormatMoney(trans.RunningBalance);
                                                                if (trans.Amount > 0)
                                                                    objTrans.Debit = FormatMoney(trans.Amount);
                                                                else
                                                                    objTrans.Credit = FormatMoney(trans.Amount);

                                                                // TODO: To confirm in release/2.0
                                                                //objTrans.Credit = trans.Credit > 0 ? FormatMoney(trans.Credit) : null;
                                                                //objTrans.Debit = trans.Debit > 0 ? FormatMoney(trans.Debit) : null;
                                                                objMonthlyReport.TransactionList.Add(objTrans);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }

                                                #region Generate PDF
                                                var monthlyDocument = new DocumentGenerator.Document
                                                {
                                                    Data = objMonthlyReport,
                                                    Format = DocumentFormat.Pdf,
                                                    Name = monthSummary.PDFReportName,
                                                    Version = "1.0"
                                                };
                                                //Logger.Info($"Document Generator Called For EntityId : {entityId} , AccountID : {item.AccountID}, Month : {objMonthlyReport.MonthlySummary.Name}, Year : {objMonthlyReport.MonthlySummary.Year}");
                                                var monthlyResult = await documentGeneratorService.Generate("CashflowMonthlySummaryReport", "1.0", Format.Html, monthlyDocument);

                                                if (monthlyResult != null)
                                                {
                                                    //Logger.Info($"Application Document Called For EntityId : {entityId} , AccountID : {item.AccountID}, Month : {objMonthlyReport.MonthlySummary.Name}, Year : {objMonthlyReport.MonthlySummary.Year}");
                                                    var objMonthlyDocument = await applicationDocumentService.Add(entityId, cashflowConfigurationService.CashflowReportConfig.Category, monthlyResult.Content, monthlyDocument.Name, null, null);
                                                    if (objMonthlyDocument != null)
                                                    {
                                                        lstMonthlyDocumentIds.Add(objMonthlyDocument.DocumentId);
                                                    }
                                                }
                                                #endregion
                                            }

                                            #region Merge PDF

                                            if (lstMonthlyDocumentIds != null && lstMonthlyDocumentIds.Count > 0)
                                            {
                                                var streams = GetStreamsToMerge(entityId, DocumentManagerService, lstMonthlyDocumentIds);
                                                var mergedPdf = PdfMergerService.MergePdf(streams);

                                                if (mergedPdf != null)
                                                {
                                                    await applicationDocumentService.Add(entityId, cashflowConfigurationService.CashflowReportConfig.Category, mergedPdf, item.AccountNumber + "_Monthly.pdf", null, null);
                                                    Logger.Info($"Merged document created for entityId : " + entityId + " AccountId : " + item.AccountNumber);
                                                }
                                            }

                                            #endregion

                                            #region Generate monthly grid report
                                            if (item.CashFlow.GridReport != null)
                                            {
                                                var gridReportTemplate = new GridReportTemplate();
                                                gridReportTemplate.ReportList = new List<GridReport>();
                                                if (item.CashFlow.GridReport.ReportList != null && item.CashFlow.GridReport.ReportList.Count > 0)
                                                {
                                                    gridReportTemplate.AccountHolderName = AccountHolderName;
                                                    gridReportTemplate.AccountType = item.CashFlow.GridReport.AccountType;
                                                    foreach (var report in item.CashFlow.GridReport.ReportList)
                                                    {
                                                        var objGridReport = new GridReport();
                                                        objGridReport.ReportHeader = report.ReportHeader;
                                                        objGridReport.GridData = new List<GridDataTemplate>();
                                                        foreach (var reportList in report.GridData)
                                                        {
                                                            var gridData = new GridDataTemplate();
                                                            if (!string.IsNullOrEmpty(reportList.GridRowHeader))
                                                            {
                                                                if (reportList.GridRowHeader.Contains("%"))
                                                                {
                                                                    gridData.GridRowHeader = reportList.GridRowHeader;
                                                                    gridData.Deposits = FormatPercentage(reportList.ColumnValue.Deposits);
                                                                    gridData.DepositCount = FormatPercentage(reportList.ColumnValue.DepositCount) + "";
                                                                    gridData.DepositAverage = FormatPercentage(reportList.ColumnValue.DepositAverage);
                                                                    gridData.NegativeDays = FormatPercentage(reportList.ColumnValue.NegativeDays) + "";
                                                                    gridData.NSF = FormatPercentage(reportList.ColumnValue.NSF) + "";
                                                                    gridData.BeginningBalance = FormatPercentage(reportList.ColumnValue.BeginningBalance);
                                                                    gridData.EndingBalance = FormatPercentage(reportList.ColumnValue.EndingBalance);
                                                                    gridData.IncDec = FormatPercentage(reportList.ColumnValue.IncDec);
                                                                    gridData.ADB = FormatPercentage(reportList.ColumnValue.ADB);
                                                                    gridData.ADBPercentage = FormatPercentage(reportList.ColumnValue.ADBPercentage);
                                                                    objGridReport.GridData.Add(gridData);
                                                                }
                                                                else
                                                                {
                                                                    gridData.GridRowHeader = reportList.GridRowHeader;
                                                                    gridData.Deposits = FormatMoney(reportList.ColumnValue.Deposits);
                                                                    gridData.DepositCount = reportList.ColumnValue.DepositCount + "";
                                                                    gridData.DepositAverage = FormatMoney(reportList.ColumnValue.DepositAverage);
                                                                    gridData.NegativeDays = reportList.ColumnValue.NegativeDays + "";
                                                                    gridData.NSF = reportList.ColumnValue.NSF + "";
                                                                    gridData.BeginningBalance = FormatMoney(reportList.ColumnValue.BeginningBalance);
                                                                    gridData.EndingBalance = FormatMoney(reportList.ColumnValue.EndingBalance);
                                                                    gridData.IncDec = FormatMoney(reportList.ColumnValue.IncDec);
                                                                    gridData.ADB = FormatMoney(reportList.ColumnValue.ADB);
                                                                    gridData.ADBPercentage = FormatPercentage(reportList.ColumnValue.ADBPercentage);
                                                                    objGridReport.GridData.Add(gridData);
                                                                }
                                                            }
                                                        }
                                                        gridReportTemplate.ReportList.Add(objGridReport);
                                                    }

                                                    var monthlyGridDocument = new DocumentGenerator.Document
                                                    {
                                                        Data = gridReportTemplate,
                                                        Format = DocumentFormat.Pdf,
                                                        Name = item.CashFlow.GridReport.PDFReportName,
                                                        Version = "1.0"
                                                    };
                                                    //Logger.Info($"Document Generator Called For EntityId : {entityId} , AccountID : {item.AccountID}, Month : {objMonthlyReport.MonthlySummary.Name}, Year : {objMonthlyReport.MonthlySummary.Year}");
                                                    var monthlyGridResult = await documentGeneratorService.Generate("CashflowMonthlyGridReport", "1.0", Format.Html, monthlyGridDocument);

                                                    if (monthlyGridResult != null)
                                                    {
                                                        //Logger.Info($"Application Document Called For EntityId : {entityId} , AccountID : {item.AccountID}, Month : {objMonthlyReport.MonthlySummary.Name}, Year : {objMonthlyReport.MonthlySummary.Year}");
                                                        await applicationDocumentService.Add(entityId, cashflowConfigurationService.CashflowReportConfig.Category, monthlyGridResult.Content, monthlyGridDocument.Name, null, null);
                                                    }
                                                }
                                            }

                                            #endregion
                                        }
                                        #endregion
                                    }

                                    if (cashflowConfigurationService.CashflowReportConfig.Consolidated.Transactions)
                                    {
                                        #region Transaction List                                       

                                        if (lstTransactionList != null && lstTransactionList.Count > 0)
                                        {
                                            lstTransactionList = lstTransactionList.OrderByDescending(i => i.TransactionOn.Time).ToList();
                                            objCashflowReport.TransactionList = new List<CashflowTransactionListSection>();
                                            foreach (var trans in lstTransactionList)
                                            {
                                                var objTrans = new CashflowTransactionListSection();
                                                objTrans.Amount = FormatMoney(trans.Amount);
                                                objTrans.Description = trans.Description;
                                                objTrans.Date = trans.TransactionOn.Time.ToString("MM/dd/yyyy");
                                                objTrans.TransactionType = trans.Amount > 0 ? "Debit" : "Credit";
                                                objTrans.RunningBalance = FormatMoney(trans.RunningBalance);
                                                objCashflowReport.TransactionList.Add(objTrans);
                                            }
                                        }
                                        #endregion
                                    }

                                    if (cashflowConfigurationService.CashflowReportConfig.Consolidated.RecurringTransactions)
                                    {
                                        #region Recurring Transactions
                                        objCashflowReport.RecurringTransaction = new List<RecurringTransactionSection>();
                                        if (item.CashFlow.RecurringList != null && item.CashFlow.RecurringList.Count > 0)
                                        {
                                            foreach (var rt in item.CashFlow.RecurringList)
                                            {
                                                if (!string.IsNullOrEmpty(rt.Name))
                                                {
                                                    var objRecurring = new RecurringTransactionSection();
                                                    objRecurring.Amount = FormatMoney(rt.Amount);
                                                    objRecurring.RoudingAmount = FormatMoney(rt.RoudingAmount);
                                                    objRecurring.Name = rt.Name;
                                                    objRecurring.TotalTransactions = rt.TotalTransactions;
                                                    objRecurring.Transactions = new List<CashflowTransactionListSection>();
                                                    foreach (var trans in rt.Transactions)
                                                    {
                                                        var objTrans = new CashflowTransactionListSection();
                                                        objTrans.Amount = FormatMoney(trans.Amount);
                                                        objTrans.Description = trans.Description;
                                                        objTrans.Date = trans.Date;
                                                        objTrans.TransactionType = trans.TransactionType;
                                                        objTrans.Credit = trans.Credit > 0 ? FormatMoney(trans.Credit) : null;
                                                        objTrans.Debit = trans.Debit > 0 ? FormatMoney(trans.Debit) : null;
                                                        objRecurring.Transactions.Add(objTrans);
                                                    }
                                                    objRecurring.ConfidenceLevel = rt.ConfidenceLevel;
                                                    objCashflowReport.RecurringTransaction.Add(objRecurring);
                                                }
                                            }
                                        }
                                        #endregion
                                    }

                                    var document = new DocumentGenerator.Document
                                    {
                                        Data = objCashflowReport,
                                        Format = DocumentFormat.Pdf,
                                        Name = item.CashFlow.TransactionSummary.PDFReportName,
                                        Version = "1.0"
                                    };
                                    try
                                    {
                                        Logger.Info($"Started Document Generator Request For EntityId : {entityId} , AccountID : {item.AccountID}");
                                        var documentResult = await documentGeneratorService.Generate("CashflowSummaryReport", "1.0", Format.Html, document);
                                        Logger.Info($"Ended Document Generator Request EntityId : {entityId} , AccountID : {item.AccountID}");
                                        if (documentResult != null)
                                        {
                                            Logger.Info($"Started Application Document Request For EntityId : {entityId} , AccountID : {item.AccountID}");
                                            await applicationDocumentService.Add(entityId, cashflowConfigurationService.CashflowReportConfig.Category, documentResult.Content, document.Name, null, null);
                                            Logger.Info($"Ended Application Document Request For EntityId : {entityId} , AccountID : {item.AccountID}");
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error($"Cashflow PDF Generator Error For : {entityId} , AccountID : {item.AccountID}, Error : {ex.Message + ex.StackTrace}");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.Error($"CashflowCalculationCompletedHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
        }
        #endregion

        // Downloads all documents of application and returns stream list
        private IEnumerable<Stream> GetStreamsToMerge(string applicationNumber, docManager.IDocumentManagerService documentManagerService, List<string> documentIds)
        {
            IList<Stream> docStreamList = new List<Stream>();

            foreach (var docId in documentIds)
            {
                Stream docStream = documentManagerService.Download("application", applicationNumber, docId).Result;
                docStreamList.Add(docStream);
            }

            return docStreamList;
        }

        public async Task<bool> CalculateManunalCashFlowEventHandler(string entityType, string entityId, object data)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            var result = false;
            try
            {
                var CSVCashflowRequest = ExecuteRequest<CSVRequest>(data.ToString());
                if (CSVCashflowRequest.fileContent != null)
                {
                    var fileBytes = Convert.FromBase64String(CSVCashflowRequest.fileContent);

                    //convert document bytes to JSON
                    var objCSVJson = Encoding.UTF8.GetString(fileBytes);

                    var allLines = objCSVJson.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                    var header = allLines[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    var csvData = allLines.Skip(1)
                                   .Select(l => l.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                                 .Select((s, i) => new { s, i })
                                                 .ToDictionary(x => header[x.i], x => x.s));

                    var settings = new JsonSerializerSettings();
                    settings.NullValueHandling = NullValueHandling.Ignore;
                    settings.DefaultValueHandling = DefaultValueHandling.Ignore;
                    settings.StringEscapeHandling = StringEscapeHandling.Default;

                    var objCSVResponse = JsonConvert.SerializeObject(csvData, settings);
                    CSVCashflowRequest.fileContent = "";

                    var lstBankStatement = ExecuteRequest<List<BankStatement>>(objCSVResponse);

                    var payload = new { data = lstBankStatement, details = CSVCashflowRequest };

                    var ruleResult = DecisionEngineService.Execute<dynamic>("ExtractCSVData", new { payload = payload });
                    if (ruleResult != null)
                    {
                        SaveAccountAndTransaction(entityType, entityId, ruleResult, DataAttributesService, CashflowConfigurationService,
                            DocumentGeneratorService, ApplicationDocumentService, Logger, true, ProductRuleService);
                    }
                    result = true;
                }
                else
                {
                    Logger.Error($"CalculateManunalCashFlowEventHandler empty uploaded file content : {entityId}");
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"CalculateManunalCashFlowEventHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
            return result;
        }

        public async Task<bool> SaveAccountDetailsOcrolusHandler(string entityType, string entityId,  IEventRequest request)
        {
            Logger.Info("Started SaveAccountDetailsOcrolusHandler:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (request.data == null)
                throw new ArgumentNullException(nameof(request.data));
            var result = false;
            try
            {
                    var productId = string.Empty;
                    var objApplicationDataAttribute = await DataAttributesService.GetAttribute(entityType, entityId, "product");
                    if (objApplicationDataAttribute != null)
                    {
                        var lstApplicationResult = (JArray)objApplicationDataAttribute;
                        if (lstApplicationResult != null && lstApplicationResult.Count > 0)
                        {
                            var json = (JObject)JsonConvert.DeserializeObject(lstApplicationResult[0] + "");

                            Dictionary<string, object> d = new Dictionary<string, object>(json.ToObject<IDictionary<string, object>>(), StringComparer.CurrentCultureIgnoreCase);

                            productId = d["ProductId"] + "";
                        }
                    }
                var objRuleResult = await ProductRuleService.RunRule(entityType, entityId, productId, request.DERuleName,request.data);
                var bankaccount = ExecuteRequest<BankAccount>(objRuleResult.IntermediateData.ToString());
                bankaccount.EntityId = entityId;
                bankaccount.EntityType = entityType;
                var accountId=await AccountRepository.AddBankAccount(bankaccount);
                Object ocrolusData = new { AccountId = accountId, Data = request.data };
                var objRuleResultOcrolus = await ProductRuleService.RunRule(entityType, entityId, productId, "GenerateOcrolusCashflow", ocrolusData);
                if (objRuleResultOcrolus != null && objRuleResultOcrolus.IntermediateData != null)
                {
                    var intermediateResult = JsonConvert.DeserializeObject<Dictionary<string, object>>(objRuleResultOcrolus.IntermediateData.ToString());
                    if (intermediateResult != null && intermediateResult.Any())
                    {
                        foreach (var attribute in intermediateResult)
                        {
                            await DataAttributesService.SetAttribute(entityType, entityId, CashflowConfigurationService.DataAttributeStoreName, attribute.Value,accountId);
                        }
                    }
                }
                Logger.Info($"SaveAccountDetailsOcrolusHandler ended For : {entityId} ");
            }
            catch (Exception ex)
            {
                Logger.Error($"SaveAccountDetailsOcrolusHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
            return result;
        }

        public async Task<IAccountTypeResponse> GetAccountByType(string entityType, string entityId, IAccountTypeRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lstResult = new AccountTypeResponse();
            try
            {
                Logger.Info("Started Execution for GetAccountByType at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

                lstResult.accounts = new List<IBankAccount>();
                if (request.IsCashflowAccount.HasValue && request.IsCashflowAccount.Value)
                {
                    try
                    {
                        var cashflowAccount = await AccountRepository.GetCashflowAccount(entityType, entityId);
                        if (cashflowAccount != null)
                        {
                            lstResult.accounts.Add(cashflowAccount);
                        }
                    }
                    catch
                    {
                    }
                }
                if (request.IsFundingAccount.HasValue && request.IsFundingAccount.Value)
                {
                    try
                    {
                        var fundingAccount = await AccountRepository.GetFundingAccount(entityType, entityId);
                        if (fundingAccount != null)
                        {
                            lstResult.accounts.Add(fundingAccount);
                        }
                    }
                    catch { }
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetAccountByType Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + exception.StackTrace);
            }

            Logger.Info("Completed Execution for GetAccountByType Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "Service: Cashflow");
            return lstResult;
        }

        #region Finicity Handlers        

        #region UserAction Handler
        public async Task<bool> FinicityUserActionHandler(string entityType, string entityId, IEventRequest request)
        {
            bool result = false;
            try
            {
                Logger.Info("Started Execution for FinicityUserActionHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (request == null)
                    throw new ArgumentNullException(nameof(request));
                if (request.data == null)
                    throw new ArgumentNullException(nameof(request.data));
                var ruleResult = DecisionEngineService.Execute<dynamic>(request.DERuleName, new { input = request.data });
                if (ruleResult != null)
                {
                    UserActions objData = new UserActions();
                    objData = ExecuteRequest<UserActions>(ruleResult + "");
                    if (objData != null)
                    {
                        foreach (var item in objData.CustomerDetails)
                        {
                            var objAccount = await AccountRepository.GetAccountByProvideAccountId(entityType, entityId, item.ProviderAccountId);
                            if (objAccount != null)
                            {
                                objAccount.Refreshstatus = false;
                                objAccount.ProviderErrorCode = item.AggregationStatusCode;
                                objAccount.ProviderErrorDescription = item.AggregationStatusDescription;
                                if (item.AggregationAttemptDate != null)
                                {
                                    objAccount.LastRefreshDate = new TimeBucket(item.AggregationAttemptDate.Value);
                                }
                                else
                                {
                                    objAccount.LastRefreshDate = new TimeBucket(TenantTime.Now);
                                }
                                objAccount.AvailableBalance = item.AvailableBalance;
                                objAccount.CurrentBalance = item.CurrentBalance;
                                if (item.BalanceDate != null)
                                {
                                    objAccount.BalanceAsOfDate = new TimeBucket(item.BalanceDate.Value);
                                }
                                else
                                {
                                    objAccount.BalanceAsOfDate = new TimeBucket(TenantTime.Now);
                                }
                                await UpdateRefreshStatus(entityType, entityId, objAccount);
                            }
                        }
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error($"FinicityUserActionHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
            Logger.Info("Completed Execution for FinicityUserActionHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }
        #endregion

        #region InBound Hook Invoked Handler

        public async Task<bool> InBoundHookInvokedHandler(string entityType, string entityId, IEventRequest request)
        {
            bool result = false;
            try
            {
                Logger.Info("Started Execution for InBoundHookInvokedHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (request == null)
                    throw new ArgumentNullException(nameof(request));
                if (request.data == null)
                    throw new ArgumentNullException(nameof(request.data));

                var ruleResult = DecisionEngineService.Execute<dynamic>(request.DERuleName, new { input = request.data });
                if (ruleResult != null)
                {
                    await UpdateAccountAndSaveTransaction(entityType, entityId, ruleResult);
                }
                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error($"InBoundHookInvokedHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
            Logger.Info("Completed Execution for InBoundHookInvokedHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }

        #endregion

        #endregion

        private async Task<string> UpdateRefreshStatus(string entityType, string entityId, IBankAccount request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (string.IsNullOrWhiteSpace(request.Source))
            {
                request.Source = CashflowConfigurationService.Source;
            }
            if (request.BalanceAsOfDate == null)
            {
                request.BalanceAsOfDate = new TimeBucket(TenantTime.Now.UtcDateTime);
            }

            request.UpdatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
            request.UpdatedBy = EnsureCurrentUser();
            if (string.IsNullOrEmpty(request.Id))
            {
                request.CreatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
                request.CreatedBy = EnsureCurrentUser();
            }
            else
            {
                await AccountHistoryRepository.UpdateAccountHistory(request);
            }

            var accountId = await AccountRepository.AddBankAccount(request);

            return accountId;
        }

        private async Task<string> UpdateBankBalance(IBankAccount request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (request.BalanceAsOfDate == null)
            {
                request.BalanceAsOfDate = new TimeBucket(TenantTime.Now.UtcDateTime);
            }

            request.UpdatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
            request.UpdatedBy = EnsureCurrentUser();
            request.Refreshstatus = true;
            request.LastRefreshDate = new TimeBucket(TenantTime.Now.UtcDateTime);
            request.ProviderErrorCode = "";
            request.ProviderErrorDescription = "";

            await AccountHistoryRepository.UpdateAccountHistory(request);
            var accountId = await AccountRepository.AddBankAccount(request);

            return accountId;
        }

        private async Task UpdateAccountAndSaveTransaction(string entityType, string entityId, dynamic ruleResult)
        {
            List<string> lstAccounts = new List<string>();
            AccountsAndTransactions objInput = new AccountsAndTransactions();
            objInput = ExecuteRequest<AccountsAndTransactions>(ruleResult + "");

            var objAccountTransaction = new List<Transaction>();

            if (objInput.Accounts != null && objInput.Accounts.Count > 0)
            {
                foreach (var account in objInput.Accounts)
                {
                    var objAccount = await AccountRepository.GetFinicityAccountByProviderAccountId(account.ProviderAccountId);

                    objAccount.CurrentBalance = account.CurrentBalance;
                    objAccount.AvailableBalance = account.AvailableBalance;
                    var accountId = await UpdateBankBalance(objAccount);
                }
            }

            if (objInput.Transactions != null && objInput.Transactions.Count > 0)
            {
                foreach (var objtransaction in objInput.Transactions)
                {
                    var objAccount = await AccountRepository.GetFinicityAccountByProviderAccountId(objtransaction.ProviderAccountId);
                    objtransaction.AccountId = objAccount.Id;
                    await AddBankTransaction(entityType, entityId, objtransaction);
                }
            }

        }

        private dynamic ReadJSONFile(string fileName)
        {
            var json = string.Empty;
            var folderPath = @"D:\Projects\LF\lf_cashflow\src\LendFoundry.Cashflow\Finicity\";
            try
            {
                using (var r = new StreamReader(folderPath + fileName))
                {
                    json = r.ReadToEnd();
                }
            }
            catch (Exception)
            {
                return "";
            }
            return JsonConvert.DeserializeObject<dynamic>(json);
        }
        public async Task<bool> VerifyFinicityEvents(string entityType, string entityId, string eventName)
        {
            var finicityRequest = string.Empty;
            var finicityResponse = string.Empty;
            if (eventName == "FinicityFetchTransactionsRequested")
            {
                finicityRequest = ReadJSONFile(@"FinicityFetchTransactionsRequested-Request.json") + "";
                finicityResponse = ReadJSONFile(@"FinicityFetchTransactionsRequested-Response.json") + "";

                await EventHub.Publish(eventName, new
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = finicityResponse,
                    Request = finicityRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
            else if (eventName == "FinicityGetCustomerAccountsRequested")
            {
                finicityRequest = ReadJSONFile(@"FinicityGetCustomerAccountsRequested - Request.json") + "";
                finicityResponse = ReadJSONFile(@"FinicityGetCustomerAccountsRequested - Response.json") + "";

                await EventHub.Publish(eventName, new
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = finicityResponse,
                    Request = finicityRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
            else if (eventName == "FinicityGetACHDetailsRequested")
            {
                finicityRequest = ReadJSONFile(@"FinicityGetACHDetailsRequested - Request.json") + "";
                finicityResponse = ReadJSONFile(@"FinicityGetACHDetailsRequested - Response.json") + "";
                await EventHub.Publish(eventName, new
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = finicityResponse,
                    Request = finicityRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
            return true;
        }

        public async Task AddBankLink(string bankId, string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException($"{nameof(entityType)} cannot be null");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException($"{nameof(entityId)} cannot be null");

            var exist = await GetBankAccountDetailsById(bankId);
            if (exist == null)
                throw new ArgumentException($"Details for {bankId} not found");


            var bankLinks = await LinkRepository.GetBankLinks(bankId);
            if (bankLinks == null)
            {
                bankLinks = new BankLink();
                bankLinks.BankId = bankId;
                bankLinks.Links = new List<ILinks> { new Links { EntityId = entityId, EntityType = entityType } };
                await Task.Run(() => LinkRepository.Add(bankLinks));
            }
            else
            {
                if (bankLinks.Links != null)
                {
                    bankLinks.Links.Add(new Links { EntityId = entityId, EntityType = entityType });
                }
                else
                {
                    bankLinks.Links = new List<ILinks> { new Links { EntityId = entityId, EntityType = entityType } };
                }
                await Task.Run(() => LinkRepository.Update(bankLinks));
            }
        }

        public async Task<IBankLink> GetBankLink(string bankId)
        {
            return await LinkRepository.GetBankLinks(bankId);
        }

        public async Task<IAccountTypeResponse> GetAllAccounts(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var lstResult = new AccountTypeResponse();
            try
            {
                Logger.Info("Started Execution for GetAllAccounts Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                var allAccounts = await AccountRepository.GetAllBankAccounts(entityType, entityId);
                if (allAccounts != null && allAccounts.Count > 0)
                {
                    lstResult.accounts = allAccounts;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error While Processing GetAllAccounts Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service: Cashflow" + "Exception :" + ex.Message + "StackTrace:" + ex.StackTrace);
            }
            return lstResult;
        }

        public async Task<List<IBankAccount>> GetAccountByRoutingNumberAndAccountNumber(string routingNumber, string accountNumber)
        {
            if (string.IsNullOrWhiteSpace(routingNumber))
                throw new ArgumentNullException(nameof(routingNumber));
            if (string.IsNullOrWhiteSpace(accountNumber))
                throw new ArgumentNullException(nameof(accountNumber));

            var lstResult = new AccountTypeResponse();
            try
            {
                Logger.Info("Started Execution for GetAccountByRoutingNumberAndAccountNumber Request Info: Date & Time:" + TenantTime.Now + " RoutingNumber:" + routingNumber + " AccountNumber:" + accountNumber);
                return await Task.Run(() => AccountRepository.GetAccountByRoutingNumberAndAccountNumber(routingNumber, accountNumber));

            }
            catch (Exception ex)
            {
                Logger.Error("Error While Processing GetAccountByRoutingNumberAndAccountNumber Date & Time:" + TenantTime.Now + " RoutingNumber:" + routingNumber + " AccountNumber:" + accountNumber + "Service: Cashflow" + "Exception :" + ex.Message + "StackTrace:" + ex.StackTrace);
                return null;
            }

        }


        #region Finicity
        public async Task<bool> FinicityAccountHandler(string entityType, string entityId, IEventRequest request)
        {
            var result = false;
            try
            {
                Logger.Info("Started Execution for FinicityAccountsHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (request == null)
                    throw new ArgumentNullException(nameof(request));
                if (request.data == null)
                    throw new ArgumentNullException(nameof(request.data));

                var ruleResult = DecisionEngineService.Execute<dynamic, BankAccount>(request.DERuleName, new { input = request.data });
                if (ruleResult != null)
                    await AddBankAccount(entityType, entityId, ruleResult);
                else
                    throw new Exception($"{request.DERuleName} does not returned the account");
                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error($"FinicityAccountsHandler Error For : {entityId} , Error : {ex.ToString()}");
            }
            Logger.Info("Completed Execution for FinicityAccountsHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }

        public async Task<bool> FinicityPartialTransactionHandler(string entityType, string entityId, IEventRequest request)
        {
            var result = false;
            try
            {
                Logger.Info("Started Execution for FinicityPartialTransactionHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (request == null)
                    throw new ArgumentNullException(nameof(request));
                if (request.data == null)
                    throw new ArgumentNullException(nameof(request.data));

                var transactions = DecisionEngineService.Execute<dynamic, List<Transaction>>(request.DERuleName, new { input = request.data });

                if (transactions != null && transactions.Count > 0)
                {
                    Dictionary<string, string> accountIdsMap = new Dictionary<string, string>();

                    FaultRetry.RunWithAlwaysRetry(() =>
                    {
                        accountIdsMap.Clear();
                        var allAccounts = GetAllAccounts(entityType, entityId).Result;
                        foreach (var item in allAccounts.accounts)
                        {
                            if (!accountIdsMap.ContainsKey(item.ProviderAccountId))
                            {
                                accountIdsMap.Add(item.ProviderAccountId, item.Id);
                            }
                        }

                        if (!accountIdsMap.ContainsKey(transactions[0].ProviderAccountId))
                        {
                            throw new Exception($"Account Provider Account {transactions[0].ProviderAccountId} not created");
                        }
                    }, 3, 5);

                    foreach (var transaction in transactions)
                    {
                        transaction.AccountId = accountIdsMap[transaction.ProviderAccountId];
                        await AddBankTransaction(entityType, entityId, transaction);
                    }
                }
                else
                    throw new Exception($"{request.DERuleName} does not returned the transaction");

                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error($"FinicityPartialTransactionHandler Error For : {entityId} , Error : {ex.ToString()}");
            }
            Logger.Info("Completed Execution for FinicityPartialTransactionHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }

        public async Task<bool> FinicityAllTransactionReceivedHandler(string entityType, string entityId, IEventRequest request)
        {
            var result = false;
            try
            {
                Logger.Info("Started Execution for FinicityAllTransactionReceived Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (request == null)
                    throw new ArgumentNullException(nameof(request));
                if (request.data == null)
                    throw new ArgumentNullException(nameof(request.data));

                var response = JsonConvert.DeserializeObject<FinicityAllTransactionReceivedResponse>(JsonConvert.SerializeObject(request.data));
                List<Tuple<IBankAccount, int>> requiredBankAccounts = new List<Tuple<IBankAccount, int>>();

                FaultRetry.RunWithAlwaysRetry(() =>
                {
                    requiredBankAccounts.Clear();
                    foreach (var item in response.Response)
                    {
                        var bankAccounts = GetAllAccounts(entityType, entityId).Result;
                        var bankAccount = bankAccounts.accounts.FirstOrDefault(a => a.ProviderAccountId == item.Account.Id);
                        if (bankAccount == null)
                            throw new Exception($"Bank account information not found for ProviderId {item.Account.Id}");
                        else
                            requiredBankAccounts.Add(new Tuple<IBankAccount, int>(bankAccount, item.Count));
                    }
                }, 3, 5);


                foreach (var item in requiredBankAccounts)
                {
                    FaultRetry.RunWithAlwaysRetry(() =>
                    {
                        var transactionCount = TransactionRepository.GetTransactionCount(entityType, entityId, item.Item1.Id).Result;
                        if (transactionCount != item.Item2)
                        {
                            throw new Exception($"Transactions are not same Expected:{item.Item2} Actual:{transactionCount}");
                        }

                        CalculateCashflow(entityType, entityId, CashflowConfigurationService.RuleName["finicity"],
                            item.Item1, ProductRuleService, DataAttributesService).Wait();

                        //EventHub.Publish("FiniCityGenerateCashflow", new
                        //{
                        //    EntityId = entityId,
                        //    EntityType = entityType,
                        //    Account = item.Item1
                        //}).Wait();
                    }, 3, 5);
                }

                List<string> accountIds = requiredBankAccounts.Select(r => r.Item1.Id).ToList();

                #region PDF Creation

                if (CashflowConfigurationService.CashflowReportConfig.Consolidated.IsActive)
                {
                    await GenerateAndSaveCashflowReport(entityType, entityId, accountIds,
                          DataAttributesService,
                          CashflowConfigurationService, DocumentGeneratorService, ApplicationDocumentService, Logger);
                }
                #endregion                

                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error($"FinicityAccountsHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
            Logger.Info("Completed Execution for FinicityAccountsHandler Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }


        #endregion

        #region Calculate Running Balance

        public async Task<bool> CalculateRunningBalance(string entityType, string entityId, object data)
        {
            var accountId = string.Empty;
            var result = false;
            var transactions = new List<ITransaction>();
            try
            {
                Logger.Info("Started Execution for CalculateRunningBalance at : " + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (data == null)
                    throw new ArgumentNullException(nameof(data));

                var objAccount = JsonConvert.DeserializeObject<Dictionary<string, string>>(data.ToString());
                accountId = objAccount.FirstOrDefault().Value;

                IBankAccount account = await AccountRepository.GetAccountDetails(entityType, entityId, accountId);
                await CalculateRunningBalance(entityType, entityId, account, TransactionRepository);
                result = true;
            }
            catch (Exception ex)
            {
                Logger.Error($"CalculateRunningBalance Error For : {entityId} , AccountId : {accountId},  Error : {ex.Message + ex.StackTrace}");
            }

            return result;
        }

        private async Task<bool> CalculateRunningBalance(string entityType, string entityId, IBankAccount account,
            ITransactionRepository transactionRepository, bool updateDate = false)
        {
            var transactions = new List<ITransaction>();
            try
            {
                Logger.Info("Started Execution for CalculateRunningBalance at : " + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (account == null)
                    throw new ArgumentNullException(nameof(account));
                var endingBalance = account.CurrentBalance;
                var beginingBalance = endingBalance;

                transactions = await transactionRepository.GetAllBankTransaction(entityType, entityId, account.Id);
                if (transactions != null && transactions.Count > 0)
                {
                    if (updateDate == true)
                    {
                        foreach (var item in transactions)
                        {
                            var TransactionDate_OffSet = Convert.ToDateTime(item.TransactionDate);
                            item.TransactionOn = new TimeBucket(TransactionDate_OffSet);
                            await AddBankTransaction(entityType, entityId, item);
                        }
                        transactions = await transactionRepository.GetAllBankTransaction(entityType, entityId, account.Id);
                    }

                    if (account.Source.ToLower() == "plaid" || account.Source.ToLower() == "csv")
                    {
                        var lstTemptransactions = new List<ITransaction>();
                        lstTemptransactions = transactions.OrderByDescending(i => i.TransactionOn.Time).ToList();
                        var EndDate = lstTemptransactions[0].TransactionDate;
                        var StartDate = lstTemptransactions[lstTemptransactions.Count - 1].TransactionDate;
                        var maxTransactionDate = Convert.ToDateTime(EndDate);
                        var currentMonth = maxTransactionDate.Month;
                        var currentYear = maxTransactionDate.Year;
                        var isFirstTransaction = true;
                        var dateOfLastTransaction = new TimeBucket();
                        while (lstTemptransactions.Count > 0)
                        {
                            var currentMonthTransactions = lstTemptransactions.Where(i => i.TransactionOn.Time.Month == currentMonth && i.TransactionOn.Time.Year == currentYear).ToList();
                            if (currentMonthTransactions != null && currentMonthTransactions.Count > 0)
                            {
                                foreach (var trans in currentMonthTransactions)
                                {
                                    var currentTransactionDate = trans.TransactionOn;
                                    var day = currentTransactionDate.Day;
                                    var calculateDailyEnding = isFirstTransaction ? isFirstTransaction : (dateOfLastTransaction.Time != currentTransactionDate.Time);
                                    if (calculateDailyEnding)
                                    {
                                        endingBalance = beginingBalance;
                                    }
                                    dateOfLastTransaction = currentTransactionDate;

                                    if (trans.Amount < 0)
                                    {
                                        beginingBalance -= Math.Abs(trans.Amount);
                                    }
                                    else
                                    {
                                        beginingBalance += trans.Amount;
                                    }
                                    isFirstTransaction = false;
                                    var objTransaction = transactions.FirstOrDefault(i => i.Id == trans.Id);
                                    objTransaction.RunningBalance = endingBalance;
                                    await transactionRepository.AddTransaction(objTransaction);
                                    lstTemptransactions.Remove(trans);
                                }
                            }
                            currentMonth -= 1;
                            if (currentMonth < 0)
                            {
                                currentYear -= 1;
                                currentMonth = 12;
                            }
                        }
                    }
                    else if (account.Source.ToLower() == "finicity")
                    {
                        var lstTemptransactions = new List<ITransaction>();
                        lstTemptransactions = transactions.OrderByDescending(i => i.TransactionOn.Time).ToList();
                        var EndDate = lstTemptransactions[0].TransactionDate;
                        var StartDate = lstTemptransactions[lstTemptransactions.Count - 1].TransactionDate;
                        var maxTransactionDate = Convert.ToDateTime(EndDate);
                        var currentMonth = maxTransactionDate.Month;
                        var currentYear = maxTransactionDate.Year;
                        var isFirstTransaction = true;
                        var dateOfLastTransaction = new TimeBucket();
                        while (lstTemptransactions.Count > 0)
                        {
                            var currentMonthTransactions = lstTemptransactions.Where(i => i.TransactionOn.Time.Month == currentMonth && i.TransactionOn.Time.Year == currentYear).ToList();
                            if (currentMonthTransactions != null && currentMonthTransactions.Count > 0)
                            {
                                foreach (var trans in currentMonthTransactions)
                                {
                                    var currentTransactionDate = trans.TransactionOn;
                                    var day = currentTransactionDate.Day;
                                    var calculateDailyEnding = isFirstTransaction ? isFirstTransaction : (dateOfLastTransaction.Time != currentTransactionDate.Time);
                                    if (calculateDailyEnding)
                                    {
                                        endingBalance = beginingBalance;
                                    }
                                    dateOfLastTransaction = currentTransactionDate;

                                    if (trans.Amount < 0)
                                    {
                                        beginingBalance += Math.Abs(trans.Amount);
                                    }
                                    else
                                    {
                                        beginingBalance -= trans.Amount;
                                    }
                                    isFirstTransaction = false;
                                    var objTransaction = transactions.FirstOrDefault(i => i.Id == trans.Id);
                                    objTransaction.RunningBalance = endingBalance;
                                    await transactionRepository.AddTransaction(objTransaction);
                                    lstTemptransactions.Remove(trans);
                                }
                            }
                            currentMonth -= 1;
                            if (currentMonth < 0)
                            {
                                currentYear -= 1;
                                currentMonth = 12;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"CalculateRunningBalance Error For : {entityId} , AccountId : {account.Id},  Error : {ex.Message + ex.StackTrace}");
            }
            return true;
        }

        public async Task<bool> CalculateRunningBalanceAll(string entityType, string entityId)
        {
            var accountId = string.Empty;
            var result = false;
            var transactions = new List<ITransaction>();
            try
            {
                Logger.Info("Started Execution for CalculateRunningBalance at : " + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                var lstAccounts = await AccountRepository.GetAllBankAccounts(entityType, entityId);
                if (lstAccounts != null && lstAccounts.Count > 0)
                {
                    foreach (var account in lstAccounts)
                    {
                        await CalculateRunningBalance(entityType, entityId, account, TransactionRepository, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"CalculateRunningBalance Error For : {entityId} , AccountId : {accountId},  Error : {ex.Message + ex.StackTrace}");
            }

            return result;
        }
        #endregion

        #region Get Transactions
        public async Task<ITransactionResponse> GetAccountTransaction(string entityType, string entityId, string accountId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrEmpty(accountId))
                throw new ArgumentNullException(nameof(accountId));

            var lstResult = new TransactionResponse();
            try
            {
                Logger.Info("Started Execution for GetAccountTransaction at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

                lstResult.transaction = new List<ITransaction>();
                lstResult.transaction = await TransactionRepository.GetAllBankTransaction(entityType, entityId, accountId);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetAccountTransaction Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message);
            }
            Logger.Info("Completed Execution for GetAccountTransaction at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return lstResult;
        }
        #endregion      
    }

    public class DataAttributeSetEvent
    {
        public DataAttribute Response { get; set; }
    }
    public class FinicityAllTransactionReceivedResponse
    {
        public List<FinicityAllTransactionReceivedRequest> Response { get; set; }
    }
    public class FinicityAllTransactionReceivedRequest
    {
        public FinicityAllTransactionReceivedAccount Account { get; set; }
        public int Count { get; set; }
    }

    public class FinicityAllTransactionReceivedAccount
    {
        public string Id { get; set; }
    }
}